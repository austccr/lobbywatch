# Lobby Watch

## Getting Started

### Installation

#### Dependencies

Major dependencies:

* Ruby
* Node
* PostgreSQL
* Redis, for sidekiq jobs
* [foreman](https://github.com/ddollar/foreman), for managing development server
* [Sphinx](https://freelancing-gods.com/thinking-sphinx/v4/installing_sphinx.html), for search

This application uses
[Nokogiri](https://nokogiri.org/tutorials/installing_nokogiri.html), if you have any issues installing the nokogiri
gem, follow [its installation guide](https://nokogiri.org/tutorials/installing_nokogiri.html).

Run the local development server, and worker server with:

```
bundle exec foreman start -e .env.development
```

### Adding users (researchers)

Only people who we're registered to add and manage items can do so.

To add new Researchers, use the rails console:

```
bin/rails console
> # Note that passwords must be at least 15 characters long
> Researcher.create(email: 'example@nice.org.au', password: `password12345678`)
> exit
```

Researchers sign in at `/researchers/sign_in`.

## Public API

This app produces a public JSON API, through which you can read Items.

**The API is versioned at v0, so expect breaking changes.**

An API response returns the last 20 results matching the query;
you can then page through results using an offset query string parameter (see
example below).

### Get Items

```
# Last 20 items added.
GET /api/v0/items.json

# Next 20 items added
GET /api/v0/items.json?offset=20
```

Returns basic information about items, including:

* `url`, the URL that the item references.
* `published_at`, when we believe this item was published.
* `created_at`, when we added this item to our collection.
* `updated_at`, when we last updated this item.

Items are sorted by the most recent `published_at` first.

## Deployment

We currently deploy to [Heroku](http://heroku.com/) using the [dpl Ruby gem](https://github.com/travis-ci/dpl#heroku).

```
dpl --provider=heroku --app=lobby-watch --api-key=$YOUR_HEROKU_API_KEY
```

Do get the necessary access, first get yourself added to our team on Heroku, `austccr`.

[Download and install the Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install).

You'll find `$YOUR_HEROKU_API_KEY` in your Heroku account settings.

Let `heroku` know the app name for your local repo with:

```
heroku login
heroku git:remote --app lobby-watch
```

## Search

We use [Thinking Sphinx](https://freelancing-gods.com/thinking-sphinx/v4/) for search.

In development, manage the Sphinx server with:

```
bin/rake ts:start
bin/rake ts:stop
bin/rake ts:restart
```

Build the initial index with:

```
bin/rake ts:index
```

After this point, ['real-time' indexes](https://freelancing-gods.com/thinking-sphinx/v4/indexing.html#realtime) are updated using a callback on Item.

This is added to Heroku using [Flying Sphinx](https://info.flying-sphinx.com/docs.html).

## Production

When working with the production deployment, first, get configured for [deployment (see above)](#deployement).

### Pass the master.key value to Heroku

This is required for accessing our encrypted credentials.

```
heroku config:set RAILS_MASTER_KEY="$(< config/master.key)"
```

### Viewing logs

Use the [Heroku logging commands](https://devcenter.heroku.com/articles/logging)
, e.g. `heroku logs` or `heroku logs --tail`.

### Rails console

```
heroku run rails console
```

### Running Migrations

```
heroku run rails db:migrate
```

Or

```
heroku run rails db:rollback
```

Then restart workers.

### Restarting Heroku workers

After running a migration, restart all the workers:

```
heroku restart
```

### Executing Tasks

Just like migrations, e.g.

```
heroku run rails "import:items_from_remote_csv_for_entity[https://gitlab.com/austccr/lobbywatch-initial-collection-files/raw/master/normalised/2019-03-19_MCA.csv, mca]"
```

### Managing Sphinx search index

```
# Index records
heroku run rake ts:index

# Start, stop, restart the sphinx daemon
heroku run rake ts:start
heroku run rake ts:stop
heroku run rake ts:restart

# Stop Sphinx, process the indices, and start up again
heroku run rake ts:rebuild
```

## Backups

Production database backups are managed through Heroku.
See the [Heroku documentation](https://devcenter.heroku.com/articles/heroku-postgres-backups#downloading-your-backups) for scheduling, downloading, and restoring PostgreSQL databases backups.

Some useful commands:

```
# View the available backups with:
heroku pg:backups

# See the backup schedule with:
heroku pg:backups:schedules

# Capture a new backup
heroku pg:backups:capture

# Restore the latest backup to your local machine
heroku pg:backups:download
bin/rails db:drop
bin/rails db:setup
pg_restore --verbose --clean --no-acl --no-owner --no-password -U luke -d lobbywatch_development latest.dump
bin/rails db:environment:set RAILS_ENV=development
bin/rails ts:rebuild
```

## Icon Credits

This project uses icons from the [Noun Project](https://blog.thenounproject.com/), including:

* [Open external](https://thenounproject.com/raphaelbuquet/uploads/?i=1126722), by Raphaël Buquet from the Noun Project
