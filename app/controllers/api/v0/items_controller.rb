require 'json_serializer'

class Api::V0::ItemsController < ApplicationController
  before_action(:authenticate_api_feed!)

  def index
    @entity = Entity.find_by_slug(item_filter_params[:entity]) if item_filter_params[:entity]
    @items = ItemSearcher.new(
      filters: ItemSearcher.format_filters(item_filter_params, @entity),
      current_page: params[:page],
      per_page: 20
    ).search

    respond_to do |format|
      format.json { render json: JsonSerializer::Item.serialize(@items) }
    end
  end

  private

  def item_filter_params
    params.permit [
      :tag,
      :type,
      :year,
      :q,
      :entity
    ]
  end

  def authenticate_api_feed!
    render(plain: 'Unauthorised (401)', status: 401) unless authenticate_feed_key
  end

  def api_params
    params.permit(:offset)
  end
end
