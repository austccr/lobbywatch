class ApplicationController < ActionController::Base
  before_action :set_raven_context
  before_action :use_minimal_layout, if: :devise_controller?
  before_action :set_paper_trail_whodunnit
  before_action :store_return_to_url

  protected

  def use_minimal_layout
    @minimal_layout = true
  end

  def user_for_paper_trail
    current_researcher&.id
  end

  def store_return_to_url
    return nil if request.referer&.include? 'sign_in'
    return nil if request.referer&.include? 'items/'
    return nil if request.referer&.include? '/edit'

    session[:return_to] = request.referer
  end

  def assign_return_to(object)
    @return_to = helpers.choose_return_url(
      referer: session[:return_to], object: object
    )
  end

  def render_notice(message, with_wrapper_par)
    render_to_string(
      partial: 'notice',
      locals: {
        message: message, with_wrapper_par: with_wrapper_par
      }
    )
  end

  def authenticate_editor!
    render(plain: 'Unauthorised (401)', status: 401) unless current_researcher&.is_editor?
  end

  def authenticate_feed_key
    return false unless self.params[:key].present?
    keys = [ENV['FEED_KEY']] + Researcher.pluck(:read_key)
    keys.include? self.params[:key]
  end

  private

  def set_raven_context
    Raven.user_context(id: current_researcher&.id)
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end
end
