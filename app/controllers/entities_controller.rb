class EntitiesController < ApplicationController
  before_action(:authenticate_researcher!)
  before_action(:authenticate_editor!)
  before_action :use_minimal_layout, only: [:new, :create, :edit, :update]

  def new
    @entity = Entity.new

    assign_return_to(@entity)
  end

  def create
    @entity = Entity.new(entity_params)

    assign_return_to(@entity)

    if @entity.save
      redirect_to @entity, notice: "Successfully created new entity"
    else
      flash.now[:error] = "We could not to save these changes."
      render :new
    end
  end

  def edit
    @entity = Entity.find_by!(slug: params[:slug])

    assign_return_to(@entity)
  end

  def update
    @entity = Entity.find_by!(slug: params[:slug])

    assign_return_to(@entity)

    if @entity.update(entity_params)
      redirect_to @entity, notice: "Successfully updated #{@entity.name}."
    else
      flash.now[:error] = "We could not to save these changes."
      render :edit
    end
  end

  private

  def entity_params
    params.require(:entity).permit([
      :name,
      :short_name,
      :description,
      :slug,
      :type
    ])
  end
end
