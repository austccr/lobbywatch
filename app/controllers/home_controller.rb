class HomeController < ApplicationController
  before_action(
    :authenticate_researcher!,
    unless: -> { ENV['PUBLIC_ACCESS'].eql?('true') && ['index'].include?(action_name) }
  )

  def index
    @industry_associations = IndustryAssociation.order(:name)
    @companies = Company.order(:name).where(promoted: true)
  end
end

