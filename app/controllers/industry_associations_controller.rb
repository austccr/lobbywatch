require 'csv_serializer'

class IndustryAssociationsController < ApplicationController
  before_action(
    :authenticate_researcher!,
    unless: -> { ENV['PUBLIC_ACCESS'].eql?('true') && ['index', 'show'].include?(action_name) }
  )
  before_action(:authenticate_editor!, except: [:show, :index])

  def index
    @industry_associations = IndustryAssociation.order(:name)
  end

  def show
    @entity = Entity.find_by!(slug: params[:slug])
    @context = @entity

    @filters = ItemSearcher.format_filters(item_filter_params, @entity)
    @include_industry_association_items = @entity.company? && !params[:company_only]
    @tag = Gutentag::Tag.find_by_name_param!(@filters[:tag]) if @filters[:tag]

    respond_to do |format|
      format.html do
        @items = ItemSearcher.new(
          filters: @filters,
          current_page: params[:page],
          with_ia_items: @include_industry_association_items
        ).search
        @days_with_items = @items.group_by { |i| i.document&.published_at }
        @membership_entities = @entity.members_display_shortlist

        render 'entities/show'
      end

      format.csv do
        @items = ItemSearcher.new(
          filters: @filters,
          per_page: 30000,
          with_ia_items: @include_industry_association_items
        ).search

        send_data(
          CsvSerializer::Item.serialize(@items),
          filename: CsvSerializer::Item.file_name(@entity.slug),
          type: 'text/csv'
        )
      end
    end
  end

  private

  def item_filter_params
    params.permit [
      :tag,
      :type,
      :year,
      :q
    ]
  end
end
