require 'tag_parser'
require 'csv_serializer'

class ItemsController < ApplicationController
  before_action(
    :authenticate_researcher!,
    unless: -> { public_access }
  )
  before_action(:authenticate_editor!, except: [:show, :index])
  before_action :use_minimal_layout, only: [:new, :edit, :create, :update]

  def show
    @item = Item.includes(:tags, :entity, :document).find(params[:id])
    @context = context_from_params
  end

  def new
    if new_item_params[:url].present? && new_item_params[:entity].present?
      pre_existing_item = Document.find_by_url(new_item_params[:url])
        .items
        .where(entity_id: Entity.find_by_slug(new_item_params[:entity]).id)
        .first

      redirect_to edit_item_path(pre_existing_item) if pre_existing_item
    end

    @item = Item.new(
      new_item_params.except(:entity, :url, :staging_item_id).merge(
        entity: Entity.find_by_slug(new_item_params[:entity])
      )
    )

    if new_item_params[:url].present?
      @item.document = Document.find_or_initialize_by(
        url: new_item_params[:url]
      )
    end

    pre_fetch_document_data
    assign_staging_item

    assign_return_to(@item.entity)
  end

  def edit
    @item = Item.find(params[:id])

    assign_return_to(@item.entity)
  end

  def create
    @item = Item.new(create_item_params.except(:staging_item_id))

    assign_return_to(@item.entity)

    if @item.save
      if create_item_params[:staging_item_id].present?
        StagingItem.find(
          create_item_params[:staging_item_id]
        ).update!(item: @item)

        redirect_to staging_path, notice: success_notice
      else
        redirect_to @item.entity, notice: success_notice
      end
    else
      flash.now[:error] = error_notice
      render :new
    end
  end

  def update
    attributes_for_item = item_params
    attributes_for_item[:tag_names] = TagParser.parse_string(
      attributes_for_item[:tag_names]
    )

    @item = Item.find(params[:id])

    assign_return_to(@item.entity)

    if @item.update(attributes_for_item.except(:staging_item_id))
      redirect_to(@item, notice: "Successfully updated.")
    else
      flash.now[:error] = "We could not to save the item."
      render :edit
    end
  end

  def index
    @filters = ItemSearcher.format_filters(item_filter_params, @entity)
    @tag = Gutentag::Tag.find_by_name_param!(@filters[:tag]) if @filters[:tag]

    respond_to do |format|
      format.html do
        @items = ItemSearcher.new(
          filters: @filters,
          current_page: params[:page]
        ).search
        @days_with_items = @items.group_by { |i| i.document&.published_at }
      end

      format.atom do
        if @entity
          @items = @entity.items.order(created_at: :desc).first(50)
        else
          @items = Item.order(created_at: :desc).first(50)
        end
      end

      format.csv do
        @items = ItemSearcher.new(
          filters: @filters,
          per_page: 30000,
          with_ia_items: @include_industry_association_items
        ).search

        send_data(
          CsvSerializer::Item.serialize(@items),
          filename: CsvSerializer::Item.file_name('items'),
          type: 'text/csv'
        )
      end
    end
  end

  private

  def item_filter_params
    params.permit [
      :tag,
      :type,
      :year,
      :q
    ]
  end

  def context_params
    params.permit(:context)
  end

  def context_from_params
    return unless context_params[:context]
    return @item.entity if context_params[:context].eql? 'entity'

    if context_params[:context].split(':').first.eql? 'tag'
      return @item.tags.find_by_name context_params[:context].split(':').last
    end
  end

  def success_notice
    render_notice(
      "Successfully added <a href='#{item_path(@item)}' title='View the item you added.'>a new item</a>. Nice work :)",
      true
    )
  end

  def error_notice
    render_notice("Sorry, we could not save this item.", true)
  end

  def new_item_params
    params.permit(
      :url,
      :published_at,
      :entity,
      :type,
      :staging_item_id
    )
  end

  def item_params
    params.require(:item).permit(
      :url,
      :published_at,
      :summary,
      :entity_id,
      :type,
      :tag_names,
      :featured_entities,
      :staging_item_id,
      document_attributes: [:url, :published_at, :id]
    )
  end

  def pre_fetch_document_data
    return unless @item.document

    unless @item.document&.extracted_data
      begin
        Timeout::timeout(2) {
          puts 'Attempting ContentExtractor#fetch_and_update time 2s'
          ContentExtractor.new(document: @item.document).fetch
        }
      rescue Timeout::Error
        puts 'Timed out'
      end
    end
  end

  def assign_staging_item
    if new_item_params[:staging_item_id].present?
      @item.staging_item = StagingItem.find(new_item_params[:staging_item_id])
    end
  end

  def create_item_params
    attributes_for_item = item_params
    attributes_for_item[:tag_names] = TagParser.parse_string(
      attributes_for_item[:tag_names]
    )

    attributes_for_item[:document_attributes][:id] = nil
    attributes_for_item
  end

  def public_access
    return true if is_feed_request? && authenticate_feed_key
    return true if ENV['PUBLIC_ACCESS'].eql?('true') && ['index', 'show'].include?(action_name)
    false
  end

  def is_feed_request?
    self.action_name.eql?('index') && self.params['format'].eql?('atom')
  end
end
