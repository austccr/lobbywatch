class MembershipsController < ApplicationController
  before_action(
    :authenticate_researcher!,
    unless: -> { ENV['PUBLIC_ACCESS'].eql?('true') && ['index'].include?(action_name) }
  )
  before_action(:authenticate_editor!, except: [:index])
  before_action :use_minimal_layout, only: [:new, :edit]

  def index
    @entity = Entity.includes(:memberships).find_by_slug!(entity_param_for_type)

    @memberships_with_entities =
      if @entity.company?
        @entity.memberships.map {|m| [m, m.industry_association] }
      elsif @entity.industry_association?
        @entity.memberships.map {|m| [m, m.company] }
      end

    @memberships_with_entities.sort! {|a,b| a[1]&.name <=> b[1]&.name }
  end

  def new
    @entity = Entity.find_by_slug!(entity_param_for_type)

    @membership =
      if @entity.company?
        Membership.new(company: @entity)
      elsif @entity.industry_association?
        Membership.new(industry_association: @entity)
      end

    assign_return_to(@entity)
  end

  def create
    @entity = Entity.find_by_slug!(entity_param_for_type)
    @membership = @entity.memberships.new(membership_params)

    assign_return_to(@entity)

    if @membership.save
      redirect_to @entity, notice: "Successfully added industry association membership"
    else
      flash.now[:error] = "We could not to save this membership."
      render :new
    end
  end

  def edit
    @membership = Membership.find(params[:id])
    @lead_entity_type = params[:t] if ['Company', 'IndustryAssociation'].include?(params[:t])
  end

  def update
    @membership = Membership.find(params[:id])

    if @membership.update(edit_membership_params)
      if params[:lead_entity_type].eql? 'Company'
        redirect_to company_memberships_path(@membership.company)
      elsif params[:lead_entity_type].eql? 'IndustryAssociation'
        redirect_to industry_association_members_path(@membership.industry_association)
      else
        flash.now[:error] = "We could not to save these changes. Copy the URL and let Luke know you saw this."
        render :edit
      end
    else
      flash.now[:error] = "We could not to save these changes."
      render :edit
    end
  end

  private

  def entity_params
    params.permit([:industry_association_slug, :company_slug])
  end

  def entity_param_for_type
    return entity_params[:company_slug] if entity_params[:company_slug]
    return entity_params[:industry_association_slug] if entity_params[:industry_association_slug]
  end

  def membership_params
    params.require(:membership).permit([
      :industry_association_id,
      :company_id
    ])
  end

  def edit_membership_params
    params.require(:membership).permit([:notes])
  end
end
