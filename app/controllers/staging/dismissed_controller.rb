class Staging::DismissedController < ApplicationController
  before_action :authenticate_researcher!

  def index
    @dismissed_items = StagingItem.left_joins(:document)
      .where(dismissed: true)
      .order('documents.published_at DESC, created_at DESC')
      .includes(:entity, :item_feed, :document)
    @days_with_items = @dismissed_items.group_by { |i| i.document&.published_at }
  end
end

