require 'staging_item_collector'

module Staging
  class FeedsController < ApplicationController
    before_action :authenticate_researcher!, except: [:webhook]
    skip_before_action :verify_authenticity_token, only: [:webhook]
    before_action(:authenticate_editor!, except: [:index, :webhook])

    def index
      @entities = Entity.order(:name)
        .includes(:item_feeds)
        .sort_by {|entity| entity.item_feeds.any? ? 0 : 1 }
    end

    def webhook
      unless ItemFeed.find(params[:id]).paused?
        FetchFeedItemsJob.perform_later(params[:id])
      end

      respond_to do |format|
        format.json { render json: { status: 200 } }
      end
    end

    def activate
      ItemFeed.find(params[:id]).activate!

      redirect_to(
        staging_feeds_path,
        notice: render_notice(
          "Activated feed. We will start collecting items from it again.",
          true
        )
      )
    end

    def pause
      ItemFeed.find(params[:id]).pause!

      redirect_to(
        staging_feeds_path,
        notice: render_notice(
          "Paused feed. We will not collect items from it until it is activated again.",
          true
        )
      )
    end
  end
end
