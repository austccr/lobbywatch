class StagingController < ApplicationController
  before_action(
    :authenticate_researcher!,
    unless: -> { public_access }
  )
  before_action(:authenticate_editor!, except: [:index])

  def index
    if staging_item_filter_params[:entity]
      @entity = Entity.find_by(slug: staging_item_filter_params[:entity])
    end

    @filters = ItemSearcher.format_filters(
      staging_item_filter_params,
      @entity
    )

    @staging_items = ItemSearcher.new(
      staging: true,
      filters: @filters,
      current_page: params[:page]
    ).search

    @days_with_items = @staging_items.group_by { |i| i.document&.published_at }

    respond_to do |format|
      format.html { render }
      format.atom do
        @staging_items = @staging_items.first(50)
      end
    end
  end

  def dismiss
    @staging_item = StagingItem.find(dismiss_params['id'])

    if dismiss_params['dismiss'].eql? 'true'
      @staging_item.update!(dismissed: true)
    elsif dismiss_params['dismiss'].eql? 'false'
      @staging_item.update!(dismissed: false)
    end

    respond_to do |format|
      format.html do
        redirect_to(
          staging_path,
          notice: render_notice(
            render_to_string(partial: 'notice_for_dismiss'), false
          )
        )
      end

      format.json { render json: { status: 200 } }
    end
  end

  private

  def staging_item_filter_params
    params.permit [:type, :q, :entity]
  end

  def item_filter_params
    params.permit [:type]
  end

  def query_with_filters(filters)
    filters_query = {
      dismissed: false,
      item_id: nil
    }

    filters_query.merge!({ type: filters[:type] }) if filters[:type]
    filters_query
  end

  def dismiss_params
    params.permit(:id, :dismiss, :authenticity_token, :format)
  end

  def public_access
    return true if ENV['PUBLIC_ACCESS'].eql?('true') && ['index'].include?(action_name) 
    return true if is_feed_request? && authenticate_feed_key
    false
  end

  def is_feed_request?
    self.action_name.eql?('index') && self.params['format'].eql?('atom')
  end
end
