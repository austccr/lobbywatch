require 'csv_serializer'

class TagsController < ApplicationController
  before_action(
    :authenticate_researcher!,
    unless: -> { ENV['PUBLIC_ACCESS'].eql?('true') && ['show', 'index'].include?(action_name) }
  )
  before_action(:authenticate_editor!, except: [:index, :show])
  before_action :use_minimal_layout, only: [:edit, :update]

  def index
    @tags = Gutentag::Tag.order(taggings_count: :desc)
  end

  def show
    @tag = Gutentag::Tag.find_by_name_param!(tag_params[:name])
    @context = @tag
    @items = Item.tagged_with(names: [@tag.name]).left_joins(:document).order('documents.published_at DESC, items.created_at DESC').includes(:tags, :entity, :document)

    respond_to do |format|
      format.html do
        @days_with_items = @items.group_by { |i| i.document.published_at }
        @include_industry_association_items = true
      end

      format.csv do
        send_data(
          CsvSerializer::Item.serialize(@items),
          filename: CsvSerializer::Item.file_name(@tag.name.parameterize(preserve_case: true)),
          type: 'text/csv'
        )
      end
    end
  end

  def edit
    @tag = Gutentag::Tag.find_by_name_param!(tag_params[:name])

    assign_return_to(@tag)
  end

  def update
    @tag = Gutentag::Tag.find_by_name_param!(tag_params[:name])

    assign_return_to(@tag)

    if @tag.update(edit_tag_params)
      redirect_to tag_path(@tag), notice: "Successfully updated tag"
    else
      flash.now[:error] = "We could not to save these changes."
      render :edit
    end
  end

  def destroy
    @tag = Gutentag::Tag.find_by_name_param!(tag_params[:name])

    if @tag.destroy!
      redirect_to tags_path, notice: "Successfully deleted tag '#{@tag.name}' with #{@tag.taggings_count} connections"
    else
      flash.now[:error] = "We could not to delete this tag."
      render :edit
    end
  end

  private

  def tag_params
    params.permit(:name)
  end

  def edit_tag_params
    params.require(:gutentag_tag).permit(:name, :description)
  end
end

