module ApplicationHelper
  def meta_title(front_matter: nil)
    front_matter ||= 'Tracking Lobby Groups in Australia'
    back_matter = "#{t('project_name')}, by the #{t('project_author.short_name')}"
    "#{front_matter} — #{back_matter}".html_safe
  end

  def parse_and_sanitize_markdown(text)
    sanitize(Kramdown::Document.new(text).to_html)
  end

  def item_breadscrumb_steps_for_context(item, context = nil)
    return [:items, item] unless context
    return entity_item_breadcrum_steps(item, context) if context.is_a? Entity
    return [:tags, context, item] if context.is_a? Gutentag::Tag
  end

  def context_code(context)
    return unless context
    return 'entity' if context.is_a? Entity
    return "tag:#{context.name}" if context.is_a? Gutentag::Tag
  end

  def choose_return_url(referer: nil, object: nil)
    return referer if referer
    return root_path unless object
    return company_path(object) if object.is_a? Company
    return industry_association_path(object) if object.is_a? IndustryAssociation
    return tag_path(object) if object.is_a? Gutentag::Tag

    raise
  end

  private

  def entity_item_breadcrum_steps(item, entity)
    [
      (entity.company? ? :companies : :industry_associations),
      entity,
      item
    ]
  end
end
