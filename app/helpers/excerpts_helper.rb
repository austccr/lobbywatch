# frozen_string_literal: true

module ExcerptsHelper
  def item_summary_highlighted(item)
    if highlighted?
      parse_and_sanitize_markdown(
        item_excerpter.excerpt!(item.summary)
      )
    else
      parse_and_sanitize_markdown(item.summary)
    end
  end

  def document_title_highlighted(document)
    if highlighted?
      sanitize(
        item_excerpter.excerpt!(document.title)
      )
    else
      sanitize(document.title)
    end
  end

  def document_summary_highlighted(document)
    if highlighted?
      sanitize(
        document_text_excerpter.excerpt!(
          document.full_text
        )
      )
    else
      document.extract
    end
  end

  def tweet_text_highlighter(tweet)
    if highlighted?
      sanitize(
        document_text_excerpter.excerpt!(
          (tweet['full_text'] || tweet['text']).html_safe
        )
      )
    else
      (tweet['full_text'] || tweet['text']).html_safe
    end
  end

  private

  def highlighted?
    @filters&.dig(:q).present? || @q
  end

  def query_string
    @filters&.dig(:q) || @q
  end

  def item_excerpter
    ThinkingSphinx::Excerpter.new(
      'item_core',
      query_string,
      {
        before_match: '<em class="highlight">',
        after_match: '</em>',
        limit: 0
      }
    )
  end

  def document_text_excerpter
    ThinkingSphinx::Excerpter.new(
      'item_core',
      query_string,
      {
        before_match: '<em class="highlight">',
        after_match: '</em>',
        limit: 350,
        around: 150
      }
    )
  end
end
