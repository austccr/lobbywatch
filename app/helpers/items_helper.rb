module ItemsHelper
  def show_item_source?(item)
    item.is_a?(StagingItem) && item.item_feed
  end

  def setup_item(item)
    item.document ||= Document.new
    item
  end

  def years_for_filters
    earliest_year = Item.order('documents.published_at DESC, items.created_at DESC').includes(:document).last&.document&.published_at&.year

    earliest_year ? (earliest_year..Date.today.year).to_a.reverse : []
  end
end
