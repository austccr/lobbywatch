ThinkingSphinx::Index.define :item, :with => :real_time do
  indexes summary
  indexes document.title
  indexes document.full_text

  has created_at, type: :timestamp
  has document.published_at, as: :document_published_at, type: :timestamp
  has document.published_at.year, as: :year, type: :integer
  has entity_id, type: :integer
  has type, type: :string
  has tag_ids, type: :integer, multi: true
end
