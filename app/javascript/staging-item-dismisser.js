function toggleDismissedState(buttonForm, item) {
  if (buttonForm.classList.contains('form--dismissButton--dismissed')) {
    item.classList.remove('item--dismissed');
    buttonForm.classList.remove('form--dismissButton--dismissed')
    buttonForm.querySelector('.button').setAttribute('value', 'Dismiss');
    buttonForm.querySelector('[name="dismiss"]').setAttribute('value', 'false');
  } else {
    item.classList.add('item--dismissed');
    buttonForm.classList.add('form--dismissButton--dismissed')
    buttonForm.querySelector('.button').setAttribute('value', 'Undo dismiss');
    buttonForm.querySelector('[name="dismiss"]').setAttribute('value', 'true');
  }

  buttonForm.querySelector('.button').removeAttribute('disabled');
}

function asyncDismissItem(buttonForm, item) {
  buttonForm.querySelector('.button').setAttribute('disabled', true);

  var request = new XMLHttpRequest();

  request.open('POST', buttonForm.action + '.json', true);
  request.setRequestHeader('X-CSRF-Token', document.querySelector('meta[name=csrf-token]').content);
  request.onreadystatechange = function() {
    if (request.readyState === XMLHttpRequest.DONE) {
      if (request.status === 200) {
        toggleDismissedState(buttonForm, item);
      } else {
        // If there's some error with the request, submit the form to expose the error for now.
        buttonForm.submit();
      }
    }
  };
  request.onerror = function() {
    buttonForm.querySelector('.button').removeAttribute('disabled');
  };

  request.send(new FormData(buttonForm));
}

if (document.querySelector('#stagingItemsList')) {
  document.querySelectorAll('.item--staging').forEach(function(item){
    let buttonForm = item.querySelector('.form--dismissButton')

    buttonForm.querySelector('.button').addEventListener('click', function(event) {
      event.preventDefault();
      asyncDismissItem(buttonForm, item);
    });
  });
}
