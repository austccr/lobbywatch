# frozen_string_literal: true

class FetchExtractedContentJob < ApplicationJob
  queue_as :default

  def perform(document_id)
    ContentExtractor.new(
      document: Document.find(document_id)
    ).fetch_and_update
  end
end
