require 'staging_item_collector'

class FetchFeedItemsJob < ApplicationJob
  queue_as :default

  def perform(item_feed_id)
    StagingItemCollector.fetch_and_create_from_item_feed(
      ItemFeed.find(item_feed_id)
    )
  end
end
