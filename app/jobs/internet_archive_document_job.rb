# frozen_string_literal: true

require 'internet_archiver'

class InternetArchiveDocumentJob < ApplicationJob
  queue_as :default

  def perform(document_id, archive)
    if archive
      document = Document.find(document_id)
      response = InternetArchiver.archive(document.url)

      if response[:error].present?
        logger.debug response[:error]
      else
        document.update!(syndication_url: response[:syndication])
      end
    else
      document = Document.find(document_id)

      document.update!(
        syndication_url: InternetArchiver.archived_url(document.url)
      )
    end
  end
end
