class Company < Entity
  has_many :memberships
  has_many :industry_associations, through: :memberships
end
