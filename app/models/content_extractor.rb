# frozen_string_literal: true

require 'feed_bin'
require 'twitter_api'

class ContentExtractor
  PAYWALL_URLS = [
    'https://www.dailytelegraph.com.au/subscribe/news/1/',
    'https://www.theaustralian.com.au/subscribe/news/1/',
    'https://www.couriermail.com.au/subscribe/news/1/',
    'https://www.heraldsun.com.au/subscribe/news/1/'
  ].freeze
  PAYWALL_TITLES = ['Subscribe to read | Financial Times'].freeze

  attr_accessor :document
  attr_accessor :extracted_data
  attr_accessor :tweet_data

  def initialize(document:)
    @document = document
  end

  def fetch
    if document.is_tweet?
      @tweet_data = TwitterApi::StatusFetcher.fetch(document.tweet_id)
    else
      @extracted_data = FeedBin::ContentFetcher.fetch_extracted_content(document.url)
      document.paywalled = true if paywalled_detected?
    end

    assign_data_to_document unless document.paywalled
  end

  def fetch_and_update
    fetch

    document.save!
  end

  def paywalled_detected?
    return true if PAYWALL_URLS.include? extracted_data['url']
    PAYWALL_TITLES.include? extracted_data['title']
  end

  private

  def assign_data_to_document
    if document.data.present?
      document.data = document.data.merge(formatted_data)
    else
      document.data = formatted_data
    end
  end

  def formatted_data
    data = {}
    data.merge! formatted_tweet_data if tweet_data
    data.merge! formatted_extracted_data if extracted_data
    data
  end

  def formatted_tweet_data
    { tweet: tweet_data }
  end

  def formatted_extracted_data
    { extracted: extracted_data }
  end
end
