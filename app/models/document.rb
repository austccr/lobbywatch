class Document < ApplicationRecord
  TWEET_URL_PATTERN = /^(https\:\/\/twitter\.com\/)\w+\/(status)\//
  URL_SOURCE_MAPPING = {
    "2gb.com" => "2GB",
    "2gb.com.au" => "2GB",
    "4bc.com.au" => "4BC",
    "7news.com.au" => "7 News",
    "9news.com.au" => "Nine News",
    "abc.net.au" => "ABC",
    "adelaidenow.com.au" => "Adelaide Now",
    "afgc.org.au" => "AFGC Website",
    "afr.com" => "Australian Financial Review",
    "aigroup.com.au" => "Ai Group Website",
    "aluminium.org.au" => "AAC Website",
    "appea.com.au" => "APPEA Website",
    "asiaminer.com" => "Asian Miner",
    "au.news.yahoo.com" => "Yahoo! News",
    "australianmining.com.au" => "Australian Mining",
    "australianpowerproject.com.au" => "Australian Power Project Website",
    "bbc.com" => "BBC",
    "bca.com.au" => "BCA Website",
    "benallaensign.com.au" => "Benalla Ensign",
    "bloomberg.com" => "Bloomberg",
    "brisbanetimes.com.au" => "Brisbane Times",
    "businessinsider.com.au" => "Business Insider",
    "buzzfeed.com" => "Buzzfeed",
    "cairnspost.com.au" => "Cairns Post",
    "canberratimes.com.au" => "Canberra Times",
    "caradvice.com.au" => "CarAdvice.com",
    "cdn.aigroup.com.au" => "Ai Group Website",
    "couriermail.com.au" => "Courier Mail",
    "crikey.com.au" => "Crikey",
    "dailymercury.com.au" => "Daily Mercury",
    "dailytelegraph.com.au" => "Daily Telegraph",
    "dnaindia.com" => "DNA India",
    "energyinformationaustralia.com.au" => "EIA Website",
    "energymagazine.com.au" => "Energy Magazine",
    "energymatters.com.au" => "Energy Matters",
    "energynetworks.com.au" => "Energy Networks Australia",
    "energynewsbulletin.net" => "Energy News Bulletin",
    "engineeringnews.co.za" => "Engineering News",
    "esdnews.com.au" => "ESD News",
    "euaa.com.au" => "EUAA Website",
    "facebook.com" => "FaceBook",
    "forthefutureofourregion.org.au" => "For the Future of Our Region Website",
    "ft.com" => "Financial Times",
    "gastoday.com.au" => "Gas Today",
    "geelongadvertiser.com.au" => "Geelong Advertiser",
    "gladstoneobserver.com.au" => "gladstoneobserver",
    "grattan.edu.au" => "Grattan",
    "hbrmag.com.au" => "Hunter Business Review",
    "heraldsun.com.au" => "Herald Sun",
    "illawarramercury.com.au" => "Illawarra Mercury",
    "ipa.org.au" => "IPA Website",
    "jemena.com.au" => "Jemena Website",
    "lakemactoday.com.au" => "LakeMac Today",
    "lithgowmercury.com.au" => "Lithgow Mercury",
    "mandurahmail.com.au" => "Mandurah Mail",
    "manufacturingaustralia.com.au" => "Manufacturing Australia Website",
    "minerals.org.au" => "MCA Website",
    "mining-technology.com" => "Mining Technology",
    "mining.com" => "Mining.com",
    "miningmonthly.com" => "Mining Monthly",
    "miningreview.com.au" => "Mining Review",
    "mobile.twitter.com" => "Twitter",
    "muswellbrookchronicle.com.au" => "Muswellbrook Chronicle",
    "nationalresourcesreview.com.au" => "National Resources Review",
    "news.com.au" => "news.com.au",
    "news.yahoo.com" => "Yahoo! News",
    "northerndailyleader.com.au" => "The Northern Daily Leader",
    "northqueenslandregister.com.au" => "North Queensland Register",
    "northweststar.com.au" => "North West Star",
    "nswmining.com.au" => "NSW Mining Website",
    "ntnews.com.au" => "NT News",
    "nytimes.com" => "New York Times",
    "oilprice.com" => "OilPrice.com",
    "perthnow.com.au" => "Perth Now",
    "pimagazine-asia.com" => "Pi Magazine",
    "pipeliner.com.au" => "The Australian Pipeliner",
    "qrc.org.au" => "QRC Website",
    "reneweconomy.com.au" => "Renew Economy",
    "reuters.com" => "Reuters",
    "sbs.com.au" => "SBS",
    "skynews.com.au" => "Sky News",
    "smartcompany.com.au" => "Smart Company",
    "smh.com.au" => "Sydney Morning Herald",
    "specialreports.theaustralian.com.au" => "The Australian",
    "spectator.com.au" => "Spectator",
    "strongaustralia.net" => "Strong Australia Network Website",
    "sunshinecoastdaily.com.au" => "Sunshine Coast Daily",
    "theage.com.au" => "The Age",
    "theaustralian.com.au" => "The Australian",
    "thechronicle.com.au" => "The Toowoomba Chronicle",
    "theconversation.com" => "The Conversation",
    "theepochtimes.com" => "The Epoch Times",
    "thefifthestate.com.au" => "Fifth Estates",
    "theguardian.com" => "The Guardian",
    "theherald.com.au" => "Newcastle Herald",
    "themorningbulletin.com.au" => "The Morning Bulletin",
    "thenewdaily.com.au" => "New Daily",
    "thesaturdaypaper.com.au" => "The Saturday Paper",
    "thewest.com.au" => "The West Australian",
    "townsvillebulletin.com.au" => "Townsville Bulletin",
    "twitter.com" => "Twitter",
    "washingtonpost.com" => "Washington Post",
    "watoday.com.au" => "WA Today",
    "westernmagazine.com.au" => "Western Magazine",
    "worldcoal.com" => "World Coal",
    "xinhuanet.com" => "Xinhua",
    "youtu.be" => "YouTube",
    "youtube.com" => "YouTube"
  }.freeze

  has_many :items
  has_many :staging_items
  validates :url, uniqueness: { allow_blank: false }, presence: true
  validate :url_is_a_url

  before_create :populate_published_at_from_extracted_data
  after_create_commit :get_extracted_content_unless_present
  after_create_commit :prompt_archive_for_syndication_url

  def is_tweet?
    return true if initial_data&.dig('twitter_id')&.present?
    return true if url&.match?(TWEET_URL_PATTERN)
    return false
  end

  def has_tweet_data?
    tweet && !tweet['error']
  end

  def tweet_id
    initial_data&.dig('twitter_id') ||
      (/\d+$/.match(url)[0] if url&.match?(TWEET_URL_PATTERN))
  end

  def tweet
    data&.dig('tweet')
  end

  def has_preview?
    title.present? || tweet
  end

  def title
    extracted_title || initial_title
  end

  def extract
    extracted_extract || initial_extract
  end

  def url_host
    URI(url)&.host&.gsub(/^www\./, '')
  end

  def source
    URL_SOURCE_MAPPING.dig(url_host) || url_host
  end

  def published_at_display
    published_at&.to_date&.strftime('%e %b %Y')&.squish
  end

  def initial_data
    data&.dig('initial')
  end

  def extracted_data
    data&.dig('extracted')
  end

  def full_text
    text =
      if has_tweet_data?
        tweet['full_text'] || tweet['text']
      else
        extracted_data&.dig('content') || initial_data&.dig('content')
      end

    sanitizer = Rails::Html::FullSanitizer.new
    sanitizer.sanitize(text)
  end

  def prompt_archive_for_syndication_url
    InternetArchiveDocumentJob.perform_later(id, true)
  end

  def get_syndication_url
    InternetArchiveDocumentJob.perform_later(id, false)
  end

  private

  def sanitized_content(string)
    sanitizer = Rails::Html::FullSanitizer.new
    sanitizer.sanitize(string)
  end

  def url_is_a_url
    begin
      return if URI(url)&.scheme
    rescue URI::InvalidURIError, ArgumentError
    end

    errors.add(
      :url,
      'must be a valid URL, including the http or https'
    )
  end

  def populate_published_at_from_extracted_data
    return if published_at

    if is_tweet?
      self.published_at = tweet&.dig('created_at')
    else
      self.published_at = extracted_data&.dig('date_published')
    end
  end

  def get_extracted_content_unless_present
    FetchExtractedContentJob.perform_later(id) unless extracted_data
  end

  def initial_title
    initial_data&.dig('title') || initial_data&.dig('name')
  end

  def extracted_title
    extracted_data&.dig('title')
  end

  def initial_extract
    initial_data&.dig('summary') ||
      sanitized_content(initial_data&.dig('content'))&.truncate(240)
  end

  def extracted_extract
    extracted_data&.dig('exerpt') ||
      sanitized_content(extracted_data&.dig('content'))&.truncate(240)
  end
end
