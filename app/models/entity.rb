class Entity < ApplicationRecord
  TYPES = ['IndustryAssociation', 'Company']

  has_many :items
  has_many :staging_items
  has_many :item_feeds

  validates :name, :slug, presence: true
  validates :name, :slug, uniqueness: true
  validates :short_name, uniqueness: true, allow_blank: true
  validate :slug_matches_format

  def company?
    is_a? Company
  end

  def industry_association?
    is_a? IndustryAssociation
  end

  def to_param
    slug
  end

  def short_name_or_fallback
    short_name.presence || name
  end

  def years_covered_by_items
    # FIXME: This only works effectively for IndustryAssociations
    years = [
      items.where.not(published_at: nil).order(:published_at).select('EXTRACT(YEAR FROM published_at)::integer').first&.date_part,
      items.where.not(published_at: nil).order(:published_at).select('EXTRACT(YEAR FROM published_at)::integer').last&.date_part
    ]
    years.compact!

    return years if years.size < 2
    (years.first..years.last).to_a
  end

  private

  def slug_matches_format
    return if slug.eql? slug&.parameterize
    errors.add(
      :slug,
      'must only contain lowercase letters, numbers, dashes, and have no spaces or other characters.'
    )
  end
end
