class IndustryAssociation < Entity
  has_many :memberships
  has_many :companies, through: :memberships

  def members
    companies
  end

  def members_display_shortlist
    promoted_shortlist = companies.where(promoted: true).order(:name).first(12)
    unpromoted_shortlist = members.where(promoted: false).order(:name).first(12)

    (promoted_shortlist + unpromoted_shortlist).sort {|a,b| a.name <=> b.name }
  end
end
