class Item < ApplicationRecord
  Gutentag::ActiveRecord.call self

  has_paper_trail

  belongs_to :entity, optional: true
  belongs_to :document, optional: true
  has_one :staging_item

  accepts_nested_attributes_for :document, reject_if: :check_document
  validates_associated :document

  validates(
    :document_id,
    uniqueness: {
      scope: :entity_id,
      message: 'URL has already been collected for this lobby group',
      allow_blank: true
    }
  )

  delegate :url, to: :document, allow_nil: true

  after_save :assign_to_duplicate_staging_items
  after_save ThinkingSphinx::RealTime.callback_for(:item)

  # Overrides the use of 'type' for identifying a subclass
  def self.inheritance_column
    'kind'
  end

  def name
    "Item #" + id.to_s
  end

  def parse_and_set_type(string)
    self.type = known_types.select do |t|
      t if t.casecmp?(string)
    end.first || nil
  end

  def summary_or_tweet_text
    return summary if summary.present?
    return "\"#{document.full_text}\"" if document&.is_tweet?
  end

  def assign_to_duplicate_staging_items
    StagingItem.where(document_id: document_id, entity: entity)
      .select(&:pending_review?)
      .each { |si| si.update!(item_id: id) }
  end

  private

  def check_document(document_attributes)
    return true if document_attributes['url'].blank?
    return false if document&.url.eql? document_attributes['url']

    if existing_document = Document.where(url: document_attributes['url']).first
      self.document = existing_document
      true
    else
      false
    end
  end

  def known_types
    [
      'Media release',
      'Submission',
      'Advertisement',
      'Speech'
    ]
  end
end
