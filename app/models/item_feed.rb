require 'feed_bin'

class ItemFeed < ApplicationRecord
  TWEET_URL_PATTERN = /^(https\:\/\/twitter\.com\/)\w+\/(status)\//
  FEEDBIN_RETWEET_CONTENT_PATTERN = '<small>Retweeted by '

  validates :name, :url, presence: true
  belongs_to :entity, optional: true
  has_many :staging_items

  scope :active, -> { where(paused: false) }

  def self.is_tweet?(entry)
    return true if entry['twitter_id']
    return true if entry['url']&.match?(TWEET_URL_PATTERN)
    return false
  end

  def self.reject_retweets(entries)
    entries.reject do |entry|
      is_tweet?(entry) && entry['content']&.match?(FEEDBIN_RETWEET_CONTENT_PATTERN)
    end
  end

  def self.parse_url(url)
    return url unless url&.start_with? 'https://www.google.com/url'

    parsed_url = Rack::Utils.parse_query(URI(url).query)['url']
    Rack::Utils.escape_path(parsed_url)
  end

  def self.excluded_sites
    data = JSON.parse(Typhoeus.get(ENV['EXCLUDED_SITES_LIST_URL']).response_body)
    data.dig('feed', 'entry').collect {|e| e['gsx$baseurltoexclude']['$t']}
  end

  def self.screen_sites(entries)
    entries.reject do |entry|
      next unless entry['url']

      host = URI.parse(ItemFeed.parse_url(entry['url'])).host.gsub(/^www\./,'')
      ItemFeed.excluded_sites.include?(host)
    end
  end

  def fetch_items
    entries = ItemFeed.screen_sites(
      JSON.parse(request_feed.response_body)
    )

    ItemFeed.reject_retweets(entries).collect do |entry|
      {
        document_attributes: {
          url: ItemFeed.parse_url(entry["url"]),
          published_at: entry["published"],
          data: { initial: entry }
        },
        published_at: entry["published"],
        published_at_raw: entry["published_raw"] || entry["published"],
        entity_id: self.entity_id,
        type: self.item_type.presence || entry["type"],
        item_feed_id: self.id
      }
    end.compact
  end

  def activate!
    update!(paused: false)
  end

  def pause!
    update!(paused: true)
  end

  private

  def request_feed
    if url.start_with? FeedBin::Base.api_url_base
      Typhoeus.get(url, userpwd: FeedBin::Base.credentials_string)
    else
      Typhoeus.get(url)
    end
  end
end
