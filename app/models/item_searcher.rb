# frozen_string_literal: true

class ItemSearcher
  attr_accessor :filters_in
  attr_accessor :current_page
  attr_accessor :per_page
  attr_accessor :staging
  attr_accessor :with_ia_items # include a company's industry associations' items

  def self.format_filters(permitted_params, entity)
    {
      tag: permitted_params[:tag].presence,
      type: permitted_params[:type].presence,
      year: permitted_params[:year].presence&.to_i,
      q: permitted_params[:q].presence,
      entity_id: entity&.id
    }.compact
  end

  def initialize(filters: {}, current_page: nil, per_page: nil, with_ia_items: false, staging: false)
    @filters_in = filters
    @current_page = current_page
    @per_page = per_page || 50
    @term = filters[:q]
    @with_ia_items = with_ia_items
    @staging = staging
  end

  def search
    if staging
      StagingItem.search(
        @term,
        with: processed_filters.merge(dismissed: false, has_item: false),
        order: 'document_published_at DESC, created_at DESC',
        page: current_page,
        per_page: per_page,
        max_matches: 30000,
        select: '*',
        sql: { includes: [:entity, :document] }
      )
    else
      Item.search(
        @term,
        with: processed_filters,
        order: 'document_published_at DESC, created_at DESC',
        page: current_page,
        per_page: per_page,
        max_matches: 30000,
        select: '*',
        sql: { includes: [:tags, :entity, :document] }
      )
    end
  end

  def processed_staging_filters
    f = filters_in.except(:q)
    f.compact
  end

  def processed_filters
    f = filters_in.except(:q, :tag)
    f.merge!(add_tag_ids) if filters_in[:tag]
    f.merge!(add_industry_association_ids) if with_ia_items
    f[:year] = f[:year]&.to_i if f[:year]
    f.compact
  end

  private

  def add_tag_ids
    tag = Gutentag::Tag.find_by_name(filters_in[:tag])

    if tag
      { tag_ids: tag.id }
    else
      { tag_ids: 0 }
    end
  end

  def add_industry_association_ids
    {
      entity_id: [filters_in[:entity_id]].concat(
        Entity.find(
          filters_in[:entity_id]
        ).industry_associations.map(&:id)
      )
    }
  end
end
