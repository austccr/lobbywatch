class Membership < ApplicationRecord
  belongs_to :company
  belongs_to :industry_association

  # Don't allow duplicates
  validates(:company_id, uniqueness: { scope: :industry_association_id })
end

