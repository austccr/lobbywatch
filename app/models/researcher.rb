class Researcher < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable,
  # :registerable, :recoverable
  # see https://github.com/plataformatec/devise
  devise :database_authenticatable, :rememberable, :validatable

  validates :password, length: {minimum: 15}
end
