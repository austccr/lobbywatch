class StagingItem < ApplicationRecord
  belongs_to :entity, optional: true
  belongs_to :item_feed, optional: true
  belongs_to :document, optional: true
  belongs_to :item, optional: true

  accepts_nested_attributes_for :document, reject_if: :check_document
  validates_associated :document

  validate :is_not_already_item

  validates(
    :document_id,
    uniqueness: {
      scope: :entity_id,
      message: 'We already have an item with this URL for this lobby group',
      allow_blank: true
    }
  )
  validates_associated :document

  delegate :url, to: :document, allow_nil: true

  after_save ThinkingSphinx::RealTime.callback_for(:staging_item)

  scope :to_sort, -> { where(dismissed: false, item_id: nil) }

  # Overrides the use of 'type' for identifying a subclass
  def self.inheritance_column
    'kind'
  end

  # mock this essentially
  def tag_names
    nil
  end

  def has_item
    item_id.present?
  end

  def pending_review?
    return false if dismissed
    return false if has_item
    true
  end

  def summary_or_tweet_text
    nil # mocked for CsvSerializer
  end

  def tag_names
    nil # mocked for CsvSerializer
  end

  private

  def check_document(document_attributes)
    return true if document_attributes['url'].blank?

    if existing_document = Document.where(url: document_attributes['url']).first
      self.document = existing_document
      true
    else
      false
    end
  end

  def is_not_already_item
    return unless existing_item_for_document = document&.items.where(entity_id: entity_id).first
    return if existing_item_for_document.eql? self.item

    errors.add(
      :document,
      'we already have an item for this web page url'
    )
  end
end

