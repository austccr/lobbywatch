atom_feed(language: 'en-GB', root_url: items_url) do |feed|
  feed.title('Lobbywatch items')
  feed.updated(@items.last.created_at) if @items

  @items.each do |item|
    feed.entry(item, url: item_url(item), updated: (item.updated_at || item.created_at)) do |entry|
      entry.title("#{item.entity.name}, #{item.document&.url_host} - #{item.document&.published_at_display}")

      content = parse_and_sanitize_markdown(item.summary)
      content += content_tag(:p, 'tagged: ' + item.tag_names.join(', ')) if item.tag_names.any?
      content += content_tag(:p, link_to("View at #{item.document&.source}", item.document&.url))

      entry.content(content, type: 'html')

      entry.author do |author|
        author.name(item.entity.name)
      end
    end
  end
end
