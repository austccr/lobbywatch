atom_feed(language: 'en-GB', root_url: staging_url) do |feed|
  feed.title('Lobbywatch items in staging')
  feed.updated(@staging_items.last.created_at) if @staging_items.any?

  @staging_items.each do |item|
    feed.entry(item, url: staging_path(anchor: "item-#{item.id}") , updated: (item.published_at_raw&.to_time || item.published_at), published: (item.published_at_raw&.to_time || item.published_at)) do |entry|
      entry.title("#{item.entity.name}: #{item.summary}")

      content = content_tag(:p, "Collected: #{item.created_at.to_s}")
      content += content_tag(:p, link_to("View at #{item.document&.source}", item.document&.url))
      content += content_tag(
        :strong,
        link_to(
          'Add this item',
          new_item_path(
            url: item.document&.url,
            entity: item.entity.slug,
            type: item.type,
            published_at: item.published_at,
            staging_item_id: item.id
          ),
        )
      )

      entry.content(content, type: 'html')

      entry.author do |author|
        author.name(item.entity.name)
      end
    end
  end
end
