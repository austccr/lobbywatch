require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Lobbywatch
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
    config.active_job.queue_adapter = :sidekiq
  end
end

Raven.configure do |config|
  config.dsn = 'https://cfc272cb34f04169b46ca116bbaa5079:4bfddbbd46324129adb1bc3e45480357@sentry.io/1426922'

  # Only run Sentry in production
  config.environments = %w[ production ]

  config.silence_ready = true if Rails.env.eql? 'test'
end
