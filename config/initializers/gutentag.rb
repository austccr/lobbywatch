require 'tag'

Rails.application.config.to_prepare do
  Gutentag::Tag.include Tag
end

# Don't downcase all the tags, some of them are proper nouns
Gutentag.normaliser = lambda { |name| name }

