require 'sidekiq/web'

Rails.application.routes.draw do
  devise_for :researchers
  root to: 'home#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :items, only: [:show, :index, :new, :create, :edit, :update]

  resources :tags, only: [:index, :show, :edit, :update, :destroy], param: :name
  get 'tags/:name', to: 'tags#show', as: 'gutentag_tag'

  resources :entities, only: [:edit, :update, :new, :create], param: :slug

  resources :industry_associations, only: [:index, :show], param: :slug do
    resources :memberships, only: [:new, :create]
    resources :members, only: [:index], controller: 'memberships'
  end

  resources :companies, only: [:index, :show], param: :slug do
    resources :memberships, only: [:index, :new, :create]
  end

  resources :memberships, only: [:edit, :update]

  get 'staging', to: 'staging#index'

  namespace :staging do
    get 'dismissed', to: 'dismissed#index'

    resources :feeds, only: [:index] do
      post 'activate', on: :member, to: 'feeds#activate'
      post 'pause', on: :member, to: 'feeds#pause'
      post 'webhook', on: :member, to: 'feeds#webhook'
    end
  end

  post 'staging_items/:id/dismiss', to: 'staging#dismiss', as: 'dismiss_staging_item'

  namespace :api do
    namespace :v0 do
      resources :items, only: [:index]
    end
  end

  authenticate :researcher do
    mount Sidekiq::Web => '/sidekiq'
  end
end
