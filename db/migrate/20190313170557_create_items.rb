class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :url
      t.text :summary
      t.datetime :published_at

      t.timestamps
    end
  end
end
