class AddEntityToItem < ActiveRecord::Migration[5.2]
  def change
    add_reference :items, :entity, foreign_key: true
  end
end
