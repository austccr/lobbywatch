class AddShortNameToEntity < ActiveRecord::Migration[5.2]
  def change
    add_column :entities, :short_name, :string, null: false
  end
end
