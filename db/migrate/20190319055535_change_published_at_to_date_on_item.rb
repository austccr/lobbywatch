class ChangePublishedAtToDateOnItem < ActiveRecord::Migration[5.2]
  def change
    change_column :items, :published_at, :date
  end
end
