class AddFeaturedEntitiesToItem < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :featured_entities, :string
  end
end
