class MergeFeaturedEntityIntoTopicForItems < ActiveRecord::Migration[5.2]
  def up
    ActiveRecord::Base.transaction do
      Item.all.each do |item|
        if item.featured_entities
          unless item.topic&.include?(item.featured_entities)
            item.topic = [item.topic, item.featured_entities].compact.join(', ')
            item.save!
          end
        end
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
