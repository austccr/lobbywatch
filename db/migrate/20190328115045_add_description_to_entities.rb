class AddDescriptionToEntities < ActiveRecord::Migration[5.2]
  def change
    add_column :entities, :description, :text, default: :null
  end
end
