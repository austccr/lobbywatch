class ChangeDescriptionDefaultOnEntity < ActiveRecord::Migration[5.2]
  def up
    change_column_default :entities, :description, from: 'null', to: nil

    ActiveRecord::Base.transaction do
      Entity.where(description: 'null').each do |entity|
        entity.update!(description: nil)
      end
    end
  end

  def down
    change_column_default :entities, :description, from: nil, to: 'null'
  end
end
