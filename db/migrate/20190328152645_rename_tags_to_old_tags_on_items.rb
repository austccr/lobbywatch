# This is because we're moving to use the 'acts-as-taggable-on' gem
class RenameTagsToOldTagsOnItems < ActiveRecord::Migration[5.2]
  def change
    rename_column :items, :tags, :old_tags
  end
end
