require 'tag_parser'

class CopyItemOldTagsToTagNames < ActiveRecord::Migration[5.2]
  def up
    ActiveRecord::Base.transaction do
      Item.where.not(old_tags: nil).each do |item|
        if item.tag_names.any?
          # Uncomment for debugging
          # puts "Skipping item #{item.id}, which already has tag_names"
        else
          # Uncomment for debugging
          # puts "updating #{item.id}"
          item.update!(
            tag_names: TagParser.parse_string(item.old_tags)
          )
        end
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
