# Copied from https://github.com/paper-trail-gem/paper_trail/blob/master/lib/generators/paper_trail/install/templates/add_object_changes_to_versions.rb.erb
class AddObjectChangesToVersions < ActiveRecord::Migration[5.2]
  # The largest text column available in all supported RDBMS.
  # See `create_versions.rb` for details.
  TEXT_BYTES = 1_073_741_823

  def change
    add_column :versions, :object_changes, :text, limit: TEXT_BYTES
  end
end
