class AddTypeToItems < ActiveRecord::Migration[5.2]
  def change
    add_column :items, :type, :text, default: nil
  end
end
