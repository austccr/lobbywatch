class PopulateTypeFromSourceForItems < ActiveRecord::Migration[5.2]
  def up
    ActiveRecord::Base.transaction do
      Item.where.not(source: nil).each do |item|
        item.parse_and_set_type(item.source)
        item.save!
      end
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
