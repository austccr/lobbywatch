class AddSlugToEntity < ActiveRecord::Migration[5.2]
  def change
    add_column :entities, :slug, :string
  end
end
