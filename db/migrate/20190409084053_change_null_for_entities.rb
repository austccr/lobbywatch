class ChangeNullForEntities < ActiveRecord::Migration[5.2]
  def change
    change_column_null :entities, :short_name, true
    change_column_null :entities, :slug, false
  end
end
