class CreateMemberships < ActiveRecord::Migration[5.2]
  def change
    create_table :memberships do |t|
      t.belongs_to :trade_association, index: true, null: false
      t.belongs_to :company, index: true, null: false
      t.timestamps
    end
  end
end
