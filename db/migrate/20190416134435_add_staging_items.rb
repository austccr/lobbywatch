class AddStagingItems < ActiveRecord::Migration[5.2]
  def change
    create_table :staging_items do |t|
      t.string :url
      t.text :summary
      t.text :type
      t.date :published_at
      t.belongs_to :entity, foreign_key: true
      t.timestamps
    end
  end
end
