class AddFeeds < ActiveRecord::Migration[5.2]
  def change
    create_table :item_feeds do |t|
      t.string :name, null: false
      t.string :url, null: false
      t.belongs_to :entity, foreign_key: true
      t.timestamps
    end
  end
end
