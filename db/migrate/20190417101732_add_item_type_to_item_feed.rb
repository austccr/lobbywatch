class AddItemTypeToItemFeed < ActiveRecord::Migration[5.2]
  def change
    add_column :item_feeds, :item_type, :string
  end
end
