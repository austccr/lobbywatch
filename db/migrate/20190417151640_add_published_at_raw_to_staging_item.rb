class AddPublishedAtRawToStagingItem < ActiveRecord::Migration[5.2]
  def change
    add_column :staging_items, :published_at_raw, :string
  end
end
