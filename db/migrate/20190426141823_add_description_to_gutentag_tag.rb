class AddDescriptionToGutentagTag < ActiveRecord::Migration[6.0]
  def change
    add_column :gutentag_tags, :description, :text, default: nil
  end
end
