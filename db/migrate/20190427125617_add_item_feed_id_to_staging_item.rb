class AddItemFeedIdToStagingItem < ActiveRecord::Migration[6.0]
  def change
    add_reference :staging_items, :item_feed, foreign_key: true
  end
end
