class AddDismissedToStagingItem < ActiveRecord::Migration[6.0]
  def change
    add_column :staging_items, :dismissed, :boolean, default: false, null: false
  end
end
