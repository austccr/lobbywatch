class AddInfoUrlToItemFeed < ActiveRecord::Migration[6.0]
  def change
    add_column :item_feeds, :info_url, :string
  end
end
