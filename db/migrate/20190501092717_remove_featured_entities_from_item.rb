class RemoveFeaturedEntitiesFromItem < ActiveRecord::Migration[6.0]
  def change
    remove_column :items, :featured_entities
  end
end
