class RemoveOldTagsFromItem < ActiveRecord::Migration[6.0]
  def change
    remove_column :items, :old_tags
  end
end
