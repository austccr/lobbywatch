class AddWebPageToItem < ActiveRecord::Migration[6.0]
  def change
    add_reference :items, :web_page, foreign_key: true
  end
end
