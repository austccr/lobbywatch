class AddWebPageToStagingItem < ActiveRecord::Migration[6.0]
  def change
    add_reference :staging_items, :web_page, foreign_key: true
  end
end
