class AddItemIdToStagingItem < ActiveRecord::Migration[6.0]
  def change
    add_reference :staging_items, :item, foreign_key: true
  end
end
