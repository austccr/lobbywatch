class PopulateReverseStagingItemToItemAssociation < ActiveRecord::Migration[6.0]
  def up
    ActiveRecord::Base.transaction do
      Item.where.not(staging_item_id: nil).each do |item|
        item.staging_item.update!(
          item_id: item.id
        )
      end
    end
  end

  def down
    # Pass through
  end
end
