class RemoveReferenceToStagingItemFromItem < ActiveRecord::Migration[6.0]
  def change
    remove_reference :items, :staging_item, foreign_key: true
  end
end
