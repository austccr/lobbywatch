class AddIndexUrlToWebPage < ActiveRecord::Migration[6.0]
  def change
    add_index :web_pages, :url
  end
end
