class RemoveUrlFromItemAndStagingItem < ActiveRecord::Migration[6.0]
  def change
    remove_column :items, :url
    remove_column :staging_items, :url
  end
end
