class AddNotesToMemberships < ActiveRecord::Migration[6.0]
  def change
    add_column :memberships, :notes, :text, default: nil
  end
end
