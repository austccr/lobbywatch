class RenameTradeAssociationIdIndustryAssociationId < ActiveRecord::Migration[6.0]
  def change
    rename_column :memberships, :trade_association_id, :industry_association_id
  end
end
