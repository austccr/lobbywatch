class UpdateTypeToIndustryAssociationForEntities < ActiveRecord::Migration[6.0]
  def up
    ActiveRecord::Base.transaction do
      Entity.where(type: 'TradeAssociation').each do |entity|
        entity.update!(type: 'IndustryAssociation')
      end
    end
  end

  def Down
    ActiveRecord::Base.transaction do
      Entity.where(type: 'IndustryAssociation').each do |entity|
        entity.update!(type: 'TradeAssociation')
      end
    end
  end
end
