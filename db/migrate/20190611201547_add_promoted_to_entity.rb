class AddPromotedToEntity < ActiveRecord::Migration[6.0]
  def change
    add_column :entities, :promoted, :boolean, default: false, null: false
  end
end
