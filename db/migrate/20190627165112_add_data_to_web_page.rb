class AddDataToWebPage < ActiveRecord::Migration[6.0]
  def change
    add_column :web_pages, :data, :jsonb
  end
end
