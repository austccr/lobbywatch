class RenameWebPagesDocuments < ActiveRecord::Migration[6.0]
  def change
    rename_table 'web_pages', 'documents'
    rename_column 'items', 'web_page_id', 'document_id'
    rename_column 'staging_items', 'web_page_id', 'document_id'
  end
end
