class MovePublishedAtToDocument < ActiveRecord::Migration[6.0]
  def up
    add_column :documents, :published_at, :date

    ActiveRecord::Base.transaction do
      Document.find_each do |document|
        if document.items.any?
          document.published_at ||= document.items.first.published_at
        elsif document.staging_items.any?
          document.published_at ||= document.staging_items.first.published_at
        end

        document.save!
      end
    end
  end

  def down
    remove_column :documents, :published_at, :date
  end
end
