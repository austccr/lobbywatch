class PopulateDocumentDataWithItemPublishedAt < ActiveRecord::Migration[6.0]
  def up
    ActiveRecord::Base.transaction do
      Document.find_each do |document|
        if document.staging_items.any?
          unless document.data&.dig('published_raw')&.present?
            old_raw = document.staging_items.first.published_at_raw

            if document.data
              document.data = document.data.merge({ 'published_raw' => old_raw})
            else
              document.data = { 'published_raw' => old_raw }
            end

            document.save!
          end
        end
      end
    end
  end

  def down
    puts 'Pass through'
  end
end
