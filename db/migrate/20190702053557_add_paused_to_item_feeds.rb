class AddPausedToItemFeeds < ActiveRecord::Migration[6.0]
  def change
    add_column :item_feeds, :paused, :boolean, default: false, null: false
  end
end
