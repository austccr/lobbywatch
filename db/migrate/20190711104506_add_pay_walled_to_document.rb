class AddPayWalledToDocument < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :paywalled, :boolean, default: false, null: false
  end
end
