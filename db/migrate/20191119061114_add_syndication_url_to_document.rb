class AddSyndicationUrlToDocument < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, :syndication_url, :string
  end
end
