class AddEditorToResearcher < ActiveRecord::Migration[6.0]
  def change
    add_column :researchers, :is_editor, :boolean, null: false, default: false
  end
end
