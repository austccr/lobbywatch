class AddReadKeyToResearcher < ActiveRecord::Migration[6.0]
  def change
    add_column :researchers, :read_key, :string
  end
end
