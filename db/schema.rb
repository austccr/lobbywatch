# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_11_060951) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "documents", force: :cascade do |t|
    t.string "url", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.jsonb "data"
    t.date "published_at"
    t.boolean "paywalled", default: false, null: false
    t.string "syndication_url"
    t.index ["url"], name: "index_documents_on_url"
  end

  create_table "entities", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "short_name"
    t.text "description"
    t.string "slug", null: false
    t.string "type"
    t.boolean "promoted", default: false, null: false
  end

  create_table "gutentag_taggings", id: :serial, force: :cascade do |t|
    t.integer "tag_id", null: false
    t.integer "taggable_id", null: false
    t.string "taggable_type", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tag_id"], name: "index_gutentag_taggings_on_tag_id"
    t.index ["taggable_type", "taggable_id", "tag_id"], name: "unique_taggings", unique: true
    t.index ["taggable_type", "taggable_id"], name: "index_gutentag_taggings_on_taggable_type_and_taggable_id"
  end

  create_table "gutentag_tags", id: :serial, force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "taggings_count", default: 0, null: false
    t.text "description"
    t.index ["name"], name: "index_gutentag_tags_on_name", unique: true
    t.index ["taggings_count"], name: "index_gutentag_tags_on_taggings_count"
  end

  create_table "item_feeds", force: :cascade do |t|
    t.string "name", null: false
    t.string "url", null: false
    t.bigint "entity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "item_type"
    t.string "info_url"
    t.boolean "paused", default: false, null: false
    t.index ["entity_id"], name: "index_item_feeds_on_entity_id"
  end

  create_table "items", force: :cascade do |t|
    t.text "summary"
    t.date "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "entity_id"
    t.string "source"
    t.text "type"
    t.bigint "document_id"
    t.index ["document_id"], name: "index_items_on_document_id"
    t.index ["entity_id"], name: "index_items_on_entity_id"
  end

  create_table "memberships", force: :cascade do |t|
    t.bigint "industry_association_id", null: false
    t.bigint "company_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "notes"
    t.index ["company_id"], name: "index_memberships_on_company_id"
    t.index ["industry_association_id"], name: "index_memberships_on_industry_association_id"
  end

  create_table "researchers", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_editor", default: false, null: false
    t.string "read_key"
    t.index ["email"], name: "index_researchers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_researchers_on_reset_password_token", unique: true
  end

  create_table "staging_items", force: :cascade do |t|
    t.text "summary"
    t.text "type"
    t.date "published_at"
    t.bigint "entity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "published_at_raw"
    t.bigint "item_feed_id"
    t.boolean "dismissed", default: false, null: false
    t.bigint "document_id"
    t.bigint "item_id"
    t.index ["document_id"], name: "index_staging_items_on_document_id"
    t.index ["entity_id"], name: "index_staging_items_on_entity_id"
    t.index ["item_feed_id"], name: "index_staging_items_on_item_feed_id"
    t.index ["item_id"], name: "index_staging_items_on_item_id"
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object"
    t.datetime "created_at"
    t.text "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "item_feeds", "entities"
  add_foreign_key "items", "documents"
  add_foreign_key "items", "entities"
  add_foreign_key "staging_items", "documents"
  add_foreign_key "staging_items", "entities"
  add_foreign_key "staging_items", "item_feeds"
  add_foreign_key "staging_items", "items"
end
