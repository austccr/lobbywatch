# frozen_string_literal: true

require 'csv'

module CsvSerializer
  class Item
    CSV_HEADERS = [:id, :entity, :published_at, :type, :summary, :url, :source, :tag_names].freeze

    def self.serialize(items)
      CSV.generate(headers: true) do |csv|
        csv << CSV_HEADERS
        items.each { |item| csv << self.serialized_item(item) }
      end
    end

    def self.file_name(string)
      name = "#{Time.now.strftime('%Y%m%dT%H%M')}_"
      name += "#{string}_" if string
      name + "lobbywatch_items.csv"
    end

    private

    def self.serialized_item(item)
      [
        item.id,
        item.entity.name,
        item.document&.published_at,
        item.type,
        item.summary_or_tweet_text,
        item.document&.url,
        item.document&.source,
        (item.tag_names&.sort&.join(', ').presence)
      ]
    end
  end
end
