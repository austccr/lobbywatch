module FeedBin
  class Base
    def self.username
      Rails.application.credentials.feedbin[:username]
    end

    def self.password
      Rails.application.credentials.feedbin[:password]
    end

    def self.extract_username
      Rails.application.credentials.feedbin[:extract_username]
    end

    def self.extract_key
      Rails.application.credentials.feedbin[:extract_key]
    end

    def self.credentials_string
      "#{self.username}:#{self.password}"
    end

    def self.api_url_base
      'https://api.feedbin.com'
    end
  end
end
