module FeedBin
  class ContentFetcher
    def self.fetch_extracted_content(url)
      username = FeedBin::Base.extract_username
      secret = FeedBin::Base.extract_key
      digest = OpenSSL::Digest.new("sha1")
      signature = OpenSSL::HMAC.hexdigest(digest, secret, url)
      base64_url = Base64.urlsafe_encode64(url).gsub("\n", "")

      request_uri = URI::HTTPS.build({
        host: "extract.feedbin.com",
        path: "/parser/#{username}/#{signature}",
        query: "base64_url=#{base64_url}"
      }).to_s

      JSON.parse(Typhoeus.get(request_uri).response_body)
    end
  end
end

