# frozen_string_literal: true

class InternetArchiver
  def self.archived_url(url)
    response = JSON.parse(
      Typhoeus::Request.new("https://archive.org/wayback/available?url=#{url}", method: :get).run.response_body
    )

    response.dig('archived_snapshots', 'closest', 'url')
  end

  def self.archive(url)
    response_details = { syndication: nil, errors: nil }

    request = Typhoeus::Request.new(
      "https://web.archive.org/save/#{url}",
      method: :head
    )

    request.on_complete do |response|
      # When archive.org returns 301, it has still archived the page
      if response.success? || response.code.eql?(301)
        response_details[:syndication] = [
          "https://web.archive.org", response.headers['content-location']
        ].join
      else
        response_details[:errors] = [
          'InternetArchiver Error',
          response.code.to_s + ': ',
          response.status_message,
          response.headers.dig('x-archive-wayback-runtime-error') || response.headers.dig('X-Archive-Wayback-Runtime-Error')
        ].join
      end
    end

    request.run

    response_details
  end
end
