# frozen_string_literal: true

module JsonSerializer
  class Item
    def self.serialize(items)
      items.map do |item|
        hash = {
          id: item.id,
          type: item.type,
          summary: item.summary,
          tag_names: item.tag_names&.sort.join(', ').presence,
          created_at: item.created_at.utc.to_s,
          updated_at: item.updated_at.utc.to_s
        }

        if item.document
          hash.merge!(
            document: {
              url: item.document&.url,
              published_at: item.document&.published_at,
              source: item.document&.source,
              syndication: item.document&.syndication_url
            }
          )
        end

        if item.entity
          hash.merge!(
            entity: {
              slug: item.entity&.slug,
              name: item.entity&.name
            }
          )
        end

        hash
      end
    end
  end
end
