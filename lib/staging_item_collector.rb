# frozen_string_literal: true

class StagingItemCollector
  def self.fetch_and_create_from_item_feed(item_feed)
    item_feed.entity.staging_items.create(
      item_feed.fetch_items
    )
  end
end

