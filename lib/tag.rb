module Tag
  def self.included base
    base.extend ClassMethods
    base.send :include, InstanceMethods
  end

  module ClassMethods
    def find_by_name_param!(string)
      self.find_by_name!(string.tr('+', ' '))
    end
  end

  module InstanceMethods
    def to_param
      self.name.tr(' ', '+')
    end

    def short_description
      return unless description.present?
      html = Kramdown::Document.new(description).to_html
      sanitizer = Rails::Html::FullSanitizer.new
      sanitizer.sanitize(html).truncate_words(40)
    end
  end
end
