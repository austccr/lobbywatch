# frozen_string_literal: true

class TagParser
  def self.parse_string(string)
    return [] unless string
    tags = string.split(', ').map(&:squish)

    self.use_existing_case_if_exists(tags)
  end

  private

  def self.use_existing_case_if_exists(tags)
    tags.map do |tag|
      Gutentag::Tag.find_by(
        'lower(name) = ?', tag.downcase
      )&.name || tag
    end
  end
end
