require 'feed_bin'
require 'staging_item_collector'

namespace :import do
  desc 'Import items from a Feedbin feeds'
  task :fetch_items_from_feedbin => :environment do |task|
    ItemFeed.active.where("url LIKE ?", "#{FeedBin::Base.api_url_base}%").each do |feed|
      FetchFeedItemsJob.perform_later(feed.id)
    end
  end
end
