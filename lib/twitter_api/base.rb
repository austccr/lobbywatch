module TwitterApi
  class Base
    def self.credentials
      Rails.application.credentials.twitter
    end
  end
end
