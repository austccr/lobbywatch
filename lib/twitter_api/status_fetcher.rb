module TwitterApi
  class StatusFetcher
    def self.fetch(twitter_id)
      client = Twitter::REST::Client.new do |config|
        config.consumer_key        = TwitterApi::Base.credentials[:consumer_key]
        config.consumer_secret     = TwitterApi::Base.credentials[:consumer_secret]
      end

      begin
        client.status(twitter_id, tweet_mode: 'extended').to_hash
      rescue Twitter::Error::NotFound => e
        {
          error: e.class.to_s,
          message: e.message,
          failed: true
        }
      end
    end
  end
end
