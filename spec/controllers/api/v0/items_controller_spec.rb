require 'rails_helper'

RSpec.describe Api::V0::ItemsController, type: :controller, sphinx: :true  do
  describe 'GET index' do
    before(:each) do
      Researcher.create!(
        email: 'luke@accr.org.au',
        password: 'password12345678',
        read_key: 'correct-key'
      )
    end

    context 'when there are items' do
      before do
        23.times do |i|
          Item.create!(
            document_attributes: {
              url: "http://item#{i + 1}.org",
              published_at: Date.new(2019,4,3) - i
            }
          )
        end
      end

      it 'returns the last 20 items by published_at' do
        get :index, format: :json, params: { key: 'correct-key' }

        expect(JSON.parse(response.body).map {|i| i['document']['url'] }).to match [
          'http://item1.org',
          'http://item2.org',
          'http://item3.org',
          'http://item4.org',
          'http://item5.org',
          'http://item6.org',
          'http://item7.org',
          'http://item8.org',
          'http://item9.org',
          'http://item10.org',
          'http://item11.org',
          'http://item12.org',
          'http://item13.org',
          'http://item14.org',
          'http://item15.org',
          'http://item16.org',
          'http://item17.org',
          'http://item18.org',
          'http://item19.org',
          'http://item20.org'
        ]
      end
    end

    context 'when there are not items' do
      it 'returns an empty Array' do
        get :index, format: :json, params: { key: 'correct-key' }

        expect(response.body).to eq "[]"
      end
    end
  end
end
