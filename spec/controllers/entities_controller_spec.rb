require 'rails_helper'

RSpec.describe EntitiesController, type: :controller do
  context 'when logged out' do
    describe 'GET edit' do
      it 'redirects to login' do
        get :edit, params: {slug: 'mca'}

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'PACTH update' do
      it 'redirects to login' do
        patch :update, params: {slug: 'mca'}

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'PUT update' do
      it 'redirects to login' do
        put :update, params: {slug: 'mca'}

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end
end
