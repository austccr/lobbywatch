require 'rails_helper'

RSpec.describe ItemsController, type: :controller do
  context 'not in public access mode' do
    describe 'GET index' do
      it 'redirects to login' do
        get :index

        expect(response).to redirect_to new_researcher_session_path
      end

      context 'and format is atom' do
        it 'is unauthorized' do
          get :index, params: { format: :atom, key: 'correct_key' }

          expect(response).to have_http_status(:unauthorized)
        end

        context 'and an access key is set' do
          around do |example|
            ClimateControl.modify FEED_KEY: 'correct_key' do
              example.run
            end
          end

          it 'blocks access to the wrong key' do
            get :index, params: { format: :atom, key: 'wrong_key' }

            expect(response).to have_http_status(:unauthorized)
          end

          it 'allows access to the correct key' do
            get :index, params: { format: :atom, key: 'correct_key' }

            expect(response).to have_http_status(:success)
          end
        end
      end
    end

    describe 'GET show' do
      it 'redirects to login' do
        get :show, params: {id: 5}

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end

  context 'when logged out' do
    describe 'GET new' do
      it 'redirects to login' do
        get :new

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'GET edit' do
      it 'redirects to login' do
        get :edit, params: {id: 5}

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'POST create' do
      it 'redirects to login' do
        post :create

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'PACTH update' do
      it 'redirects to login' do
        patch :update, params: {id: 5}

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'PUT update' do
      it 'redirects to login' do
        put :update, params: {id: 5}

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end
end
