require 'rails_helper'

RSpec.describe MembershipsController, type: :controller do
  context 'not in public access mode' do
    describe 'GET index' do
      it 'redirects to login' do
        get :index, params: { company_slug: 'bhp' }

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end

  context 'when logged out' do
    describe 'GET new' do
      it 'redirects to login' do
        Company.create!(name: 'BHP', slug: 'bhp')

        get :new, params: { company_slug: 'bhp' }

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'GET edit' do
      it 'redirects to login' do
        company = Company.create!(name: 'BHP', slug: 'bhp')
        company.memberships.create(
          { id: 3, industry_association: IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca') }
        )

        get :edit, params: { id: 3 }

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'POST create' do
      it 'redirects to login' do
        Company.create!(name: 'BHP', slug: 'bhp')

        post :create, params: { company_slug: 'bhp' }

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'POST update' do
      it 'redirects to login' do
        company = Company.create!(name: 'BHP', slug: 'bhp')
        company.memberships.create(
          { id: 3, industry_association: IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca') }
        )

        post :update, params: { id: 3  }

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end
end

