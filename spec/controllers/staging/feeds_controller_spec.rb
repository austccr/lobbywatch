require 'rails_helper'

RSpec.describe Staging::FeedsController, type: :controller do
  context 'when logged out' do
    describe 'GET index' do
      it 'redirects to login' do
        get :index

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end

  describe 'GET webhook' do
    let(:feed) { ItemFeed.create!(id: 3, name: 'foo', url: 'https://feed.com/feed.json') }

    context 'when the feed is active' do
      before { feed.activate! }

      it 'creates a job to fetch items' do
        expect(FetchFeedItemsJob).to receive(:perform_later).with('3')

        post 'webhook', params: { id: 3 }, format: :json

        expect(response.status).to eq 200
        expect(response.body).to eq({ status: 200 }.to_json)
      end
    end

    context 'when the feed is paused' do
      before { feed.pause! }

      it 'does not create a job and returns success response' do
        expect(FetchFeedItemsJob).not_to receive(:perform_later).with('3')

        post 'webhook', params: { id: 3 }, format: :json

        expect(response.status).to eq 200
        expect(response.body).to eq({ status: 200 }.to_json)
      end
    end
  end
end
