require 'rails_helper'

RSpec.describe StagingController, type: :controller do
  context 'not in public access mode' do
    describe 'GET index' do
      it 'redirects to login' do
        get :index

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    context 'and format is atom' do
      it 'is unauthorized' do
        get :index, params: { format: :atom, key: 'correct_key' }

        expect(response).to have_http_status(:unauthorized)
      end

      context 'and an access key is set' do
        around do |example|
          ClimateControl.modify FEED_KEY: 'correct_key' do
            example.run
          end
        end

        it 'blocks access to the wrong key' do
          get :index, params: { format: :atom, key: 'wrong_key' }

          expect(response).to have_http_status(:unauthorized)
        end

        it 'allows access to the correct key' do
          get :index, params: { format: :atom, key: 'correct_key' }

          expect(response).to have_http_status(:success)
        end
      end
    end
  end

  context 'when logged out' do
    describe 'POST create' do
      it 'redirects to login' do
        post :dismiss, params: { id: '3' }

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end
end
