require 'rails_helper'

RSpec.describe TagsController, type: :controller do
  context 'not in public access mode' do
    describe 'GET index' do
      it 'redirects to login' do
        get :index

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'GET show' do
      it 'redirects to login' do
        get :show, params: {name: 'foo'}

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end

  context 'when logged out' do
    describe 'GET edit' do
      it 'redirects to login' do
        get :edit, params: {name: 'Foo'}

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'PACTH update' do
      it 'redirects to login' do
        patch :update, params: {name: 'Foo'}

        expect(response).to redirect_to new_researcher_session_path
      end
    end

    describe 'PUT update' do
      it 'redirects to login' do
        put :update, params: {name: 'Foo'}

        expect(response).to redirect_to new_researcher_session_path
      end
    end
  end
end
