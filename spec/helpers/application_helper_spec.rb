require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe '#item_breadscrumb_steps_for_context' do
    context 'when there is no context' do
      it 'it is the items index' do
        item = Item.new
        steps = helper.item_breadscrumb_steps_for_context(item)

        expect(steps).to eql [:items, item]
      end
    end

    context 'when the context is an entity' do
      it do
        item = Item.new(
          entity: Company.create(name: 'BHP', slug: 'bhp')
        )
        steps = helper.item_breadscrumb_steps_for_context(item, item.entity)

        expect(steps).to eql [:companies, item.entity, item]
      end
    end

    context 'when the context is a tag' do
      it do
        tag = Gutentag::Tag.create(name: 'foo')
        item = Item.create(tag_names: ['foo'])

        steps = helper.item_breadscrumb_steps_for_context(item, tag)

        expect(steps).to eql [:tags, tag, item]
      end
    end
  end

  describe '#choose_return_url' do
    context 'when there is a referer' do
      subject do
        helper.choose_return_url(referer: '/foo/bar', object: nil)
      end

      it 'returns it' do
        is_expected.to eq '/foo/bar'
      end
    end

    context 'when there is no referer' do
      context 'and an industry association' do
        let(:entity) { Entity.new(id: 1, slug: 'zap', type: 'IndustryAssociation') }

        subject do
          helper.choose_return_url(referer: nil, object: entity)
        end

        it 'returns it' do
          is_expected.to eq '/industry_associations/zap'
        end
      end

      context 'and a company' do
        let(:entity) { Entity.new(id: 1, slug: 'zap', type: 'Company') }

        subject do
          helper.choose_return_url(referer: nil, object: entity)
        end

        it 'returns it' do
          is_expected.to eq '/companies/zap'
        end
      end

      context 'and there is a tag' do
        let(:tag) { Gutentag::Tag.new(name: 'Foo Bar') }

        subject do
          helper.choose_return_url(
            referer: nil, object: tag
          )
        end

        it 'returns it' do
          is_expected.to eq tag_path(tag.name.tr(' ', '+'))
        end
      end

      context 'and no entity' do
        subject do
          helper.choose_return_url(referer: nil, object: nil)
        end

        it 'returns it' do
          is_expected.to eq root_path
        end
      end
    end
  end
end
