require 'rails_helper'

RSpec.describe FetchFeedItemsJob, type: :job do
  describe 'perform' do
    it 'runs the StagingItemCollector' do
      item_feed = ItemFeed.create!(
        id: 3,
        name: 'MCA media releases morph.io scraper',
        url: 'https://api.morph.io/austccr/mca_media_releases_scraper/data.json?key=secret&query=select%20*%20from%20%22data%22%20limit%2010'
      )
      allow(StagingItemCollector).to receive(
        :fetch_and_create_from_item_feed
      ).with(item_feed).and_return nil

      expect(StagingItemCollector).to receive(
        :fetch_and_create_from_item_feed
      ).with(item_feed)

      FetchFeedItemsJob.perform_now(3)
    end
  end
end
