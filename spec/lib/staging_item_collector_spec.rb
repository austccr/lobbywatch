# frozen_string_literal: true

require 'rails_helper'
require 'staging_item_collector'

RSpec.describe StagingItemCollector do
  describe '.fetch_and_create_staging_items' do
    it 'calls fetch_items on the feed' do
      feed = ItemFeed.create!(
        id: 3,
        name: 'MCA media releases morph.io scraper',
        url: 'https://api.morph.io/austccr/mca_media_releases_scraper/data.json?key=secret&query=select%20*%20from%20%22data%22%20limit%2010'
      )

      allow(feed).to receive_messages(
        entity: Entity.create!(slug: 'mca', id: 5, name: 'Minerals Council of Australia'),
        fetch_items: [
          {
            document_attributes: { url: "http://minerals.org.au/news/stronger-mining-industry-supports-faster-return-budget-surplus" },
            published_at: "2019-03-05 22:27:28 UTC",
            published_at_raw: "2019-03-05 22:27:28 UTC",
            entity_id: 5,
            item_feed_id: 3,
            type: 'Media release'
          },
          {
            document_attributes: { url: "http://minerals.org.au/news/mca-calls-parliament-support-regional-communities-and-jobs" },
            published_at: "2019-02-20 02:10:20 UTC",
            published_at_raw: "2019-02-20 02:10:20 UTC",
            entity_id: 5,
            item_feed_id: 3,
            type: 'Media release'
          },
          {
            document_attributes: { url: "http://minerals.org.au/news/powering-future-australian-mining-people-innovation-and-modern-education" },
            published_at: "2019-02-13 20:56:57 UTC",
            published_at_raw: "2019-02-13 20:56:57 UTC",
            entity_id: 5,
            item_feed_id: 3,
            type: 'Media release'
          }
        ]
      )

      StagingItemCollector.fetch_and_create_from_item_feed(feed)

      staging_items = feed.entity.staging_items

      expect(staging_items.first.attributes).to include(
        {
          'summary' => nil,
          'published_at' => Date.new(2019,3,5),
          'entity_id' => 5,
          'type' => 'Media release',
          'published_at_raw' => '2019-03-05 22:27:28 UTC',
          'item_feed_id' => 3
        }
      )
      expect(staging_items.first.url).to eql(
        "http://minerals.org.au/news/stronger-mining-industry-supports-faster-return-budget-surplus"
      )

      expect(staging_items.second.attributes).to include(
        {
          'summary' => nil,
          'published_at' => Date.new(2019, 2, 20),
          'entity_id' => 5,
          'type' => 'Media release',
          'published_at_raw' => '2019-02-20 02:10:20 UTC',
          'item_feed_id' => 3
        }
      )
      expect(staging_items.second.url).to eql(
        "http://minerals.org.au/news/mca-calls-parliament-support-regional-communities-and-jobs"
      )

      expect(staging_items.third.attributes).to include(
        {
          'summary' => nil,
          'published_at' => Date.new(2019,2,13),
          'entity_id' => 5,
          'type' => 'Media release',
          'published_at_raw' => '2019-02-13 20:56:57 UTC',
          'item_feed_id' => 3
        }
      )
      expect(staging_items.third.url).to eql(
         "http://minerals.org.au/news/powering-future-australian-mining-people-innovation-and-modern-education"
      )
    end
  end
end
