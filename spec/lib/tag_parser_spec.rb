# frozen_string_literal: true

require 'rails_helper'
require 'tag_parser'

RSpec.describe TagParser do
  describe '.parse_string' do
    it { expect(TagParser.parse_string(nil)).to match [] }
    it { expect(TagParser.parse_string('')).to match [] }
    it { expect(TagParser.parse_string('foo')).to match ['foo'] }
    it { expect(TagParser.parse_string('Foo Bar')).to match ['Foo Bar'] }
    it { expect(TagParser.parse_string('foo, bar')).to match ['foo', 'bar'] }
    it { expect(TagParser.parse_string('   foo, bar   ')).to match ['foo', 'bar'] }

    it 'preserves stray punctutation' do
      expect(TagParser.parse_string('#foo, bar'))
        .to match ['#foo', 'bar']
      expect(TagParser.parse_string('foo, bar,'))
        .to match ['foo', 'bar,']
    end

    context 'when tag exists with different casing' do
      before do
        Gutentag::Tag.create(name: 'FoO')
      end

      it 'normalises case against existing tags' do
        expect(TagParser.parse_string('fOo, BAR')).to match ['FoO', 'BAR']
      end
    end
  end
end
