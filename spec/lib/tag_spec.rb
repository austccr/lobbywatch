# frozen_string_literal: true

require 'rails_helper'
require 'tag'

RSpec.describe Tag do
  describe '#show_description' do
    let(:tag) { Gutentag::Tag.new }

    subject { tag.short_description }

    context 'when it has no description' do
      it { is_expected.to be_blank }
    end

    context 'when it has a description' do
      before do
        tag.description = "description with [markdown](test)\n\nand more description text in another par."
      end

      it 'uses a truncated, html stripped version' do
        is_expected.to eql "description with markdown\n\nand more description text in another par.\n"
        is_expected.not_to be_html_safe
      end
    end
  end
end
