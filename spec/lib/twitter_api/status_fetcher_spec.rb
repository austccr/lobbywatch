# frozen_string_literal: true

require 'rails_helper'
require 'twitter_api'

RSpec.describe TwitterApi::StatusFetcher do
  describe '.fetch' do
    subject do
      VCR.use_cassette('twitter', record: :once) do
        TwitterApi::StatusFetcher.fetch('1118375877941846016')[:id]
      end
    end

    it 'collects status data from the Twitter API' do
      is_expected.to eq 1118375877941846016
    end
  end
end
