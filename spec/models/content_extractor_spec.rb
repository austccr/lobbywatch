# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ContentExtractor, type: :model do
  describe '#initialize' do
    context 'without a document' do
      it do
        expect { ContentExtractor.new }.to raise_error(ArgumentError, 'missing keyword: document')
      end
    end
  end

  it 'has a document attribute' do
    doc = Document.new
    extractor = ContentExtractor.new(document: doc)

    expect(extractor.document).to eql doc
  end

  describe '#fetch_and_update' do
    let(:document) { Document.new() }
    let(:extractor) { ContentExtractor.new(document: document) }
    subject { extractor.fetch_and_update }

    context 'when document is a tweet' do
      before { document.url = 'https://twitter.com/MineralsCouncil/status/1150634646767464449' }

      let(:tweet_response) do
        {
          'created_at' => "Mon Jul 15 05:14:12 +0000 2019",
          'id' => 1150634646767464449,
          'id_str' => "1150634646767464449",
          'text' => ".@realDonaldTrump US uranium decision great news for jobs in Australia &amp; low-emissions energy in USA. This balanced… https://t.co/hieQrmAsQn",
          'truncated' => true,
          'source' => "<a href=\"http://twitter.com\" rel=\"nofollow\">Twitter Web Client</a>",
          'user' => {
            'id' => 739838958,
            'id_str' => "739838958",
            'name' => "MineralsCouncilAust",
            'screen_name' => "MineralsCouncil",
            'location' => "",
            'description' => "Official Twitter account for the Minerals Council of Australia.",
            'url' => "http://t.co/MjwnsnR6Kz",
            'created_at' => "Mon Aug 06 04:27:22 +0000 2012",
            'favourites_count' => 409
          }
        }
      end

      it 'fetches twitter data' do
        allow(document).to receive(:tweet_id).and_return('1234')
        allow(TwitterApi::StatusFetcher).to receive(:fetch).with(document.tweet_id).and_return(tweet_response)
        allow(document).to receive(:is_tweet?).and_return(:true)

        expect{ subject }.to change { document.data }
          .from(nil).to( { 'tweet' => tweet_response } )
      end
    end

    context 'when document is a regular url' do
      before { document.url = 'https://foo.org/post' }

      context 'and the data is an error' do
        let(:error_response) do
          {
            'error' => true,
            'message' => 'Resource returned a response status code of 403 and resource was instructed to reject non-200 status codes.',
            'failed' => true
          }
        end

        before do
          allow(FeedBin::ContentFetcher).to receive(:fetch_extracted_content).with(document.url).and_return(error_response)
        end

        it 'assigns it as usual' do
          expect{ subject }.to change { document.data }
            .from(nil).to( { 'extracted' => error_response } )
        end
      end

      context 'paywalled' do
        before do
          allow(extractor).to receive(:paywalled_detected?).and_return true
        end

        it 'updates the response as data["extracted"]' do
          response = { foo: 'bar' }
          allow(FeedBin::ContentFetcher).to receive(:fetch_extracted_content).with(document.url).and_return(response)

          expect{ subject }.to_not change { document.data }
          expect(document).to be_paywalled
        end
      end

      context 'unpaywalled' do
        before do
          allow(extractor).to receive(:paywalled_detected?).and_return false
        end

        it 'updates the response as data["extracted"]' do
          # Check why document.data seems to be assigned already
          response = { foo: 'bar' }
          expected_updated_data = { 'extracted' => { 'foo' => 'bar' } }
          allow(FeedBin::ContentFetcher).to receive(:fetch_extracted_content).with(document.url).and_return(response)

          expect{ subject }.to change { document.data }
            .from(nil).to( expected_updated_data )
          expect(document).to_not be_paywalled
        end
      end
    end
  end

  describe '#paywalled_detected?' do
    let(:extractor) { ContentExtractor.new(document: Document.new) }
    subject { extractor.paywalled_detected? }

    context 'by default it is false' do
      before do
        extractor.extracted_data = {
          "title" => "Brisbane-based gas company set to explore - Queensland Resources Council",
          "content" => "<div class=\"content\"> <p>The Queensland Resources Council (QRC) welcomes the Palaszczuk Government&#x2019;s granting of an Authority to Prospect to Armour Energy for exploration in the Surat Basin.</p>\n<p>QRC Chief Executive Ian Macfarlane said Brisbane-based Armour Energy would supply gas for domestic use which is another sign the Queensland gas industry is leading the nation with a proactive approach to easing the east coast gas squeeze.</p>\n<p>&#x201C;Once again we see Queensland holding the key to sustaining a long-term energy future on the east coast through exploration and new opportunities for jobs and investment,&#x201D; Mr Macfarlane said.</p>\n<p>&#x201C;I applaud the Palaszczuk Government and Mines Minister Dr Anthony Lynham for their continued support of the State&#x2019;s gas industry.</p>\n<p>&#x201C;Importantly, this is an investment into regional Queensland, where all levels of&#xA0;governments and farmers support the gas industry, resulting in massive economic benefits for farmers and rural and regional communities who are currently experiencing a heartbreaking drought.&#x201D;</p>\n<p>Armour will explore 318 square kilometres of highly prospective land in the Roma Shelf north of Miles and Surat and will use existing infrastructure close to Kincora plant to deliver the much needed gas sooner to the east coast.</p>\n<p>Mr Macfarlane said Queensland&#x2019;s neighbours must take a leaf out of our book. New South Wales and Victoria can&#x2019;t expect Queensland to continue to supply, and subsidise, their own gas users.</p>\n<p>&#x201C;It&#x2019;s time for both sides of politics to consider rewarding States that do develop their resources, at the expense of those who don&#x2019;t,&#x201D; he said.</p>\n<p>The Queensland resources sector now provides one in every six dollars in the Queensland economy, sustains one in eight Queensland jobs, and supports more than 16,400 businesses across the State all from 0.1 per cent of Queensland&#x2019;s land mass.</p>\n<p>Media contact: Anthony Donaghy 0412 450 360</p> </div>",
          "url" => "https://www.qrc.org.au/media-releases/brisbane-based-gas-company-set-to-explore/"
        }
      end

      it { is_expected.to be false }
    end

    context 'when matches theaustralia.com.au paywall' do
      before do
        extractor.extracted_data = {
          "title" => "Subscribe to The Australian | Newspaper home delivery, website, iPad, iPhone & Android apps",
          "content" => "<body> <caas-subscription-form> </caas-subscription-form> <caas-analytics> </caas-analytics> <caas-exit-popup></caas-exit-popup>\n</body>",
          "url" => "https://www.theaustralian.com.au/subscribe/news/1/",
        }
      end

      it { is_expected.to be true }
    end

    context 'when matches heraldsun.com.au paywall' do
      before do
        extractor.extracted_data = {
          "title" => "Subscribe to the Herald Sun for exclusive stories",
          "content" => "<body> <caas-subscription-form> </caas-subscription-form> <caas-analytics> </caas-analytics> <caas-exit-popup></caas-exit-popup>\n</body>",
          "url" => "https://www.heraldsun.com.au/subscribe/news/1/"
        }
      end

      it { is_expected.to be true }
    end

    context 'when matches ft.com paywall' do
      before do
        extractor.extracted_data = {
          "title" => "Subscribe to read | Financial Times",
          "url" => "https://www.ft.com/content/652fb4b6-7095-11e9-bf5c-6eeb837566c5",
          "excerpt" => "Gain a global perspective on the US and go beyond with curated news and analysis from 600 journalists in 50+ countries covering politics, business, innovation, trends and more Choose your subscription&hellip;",
        }
      end

      it { is_expected.to be true }
    end

    context 'when matches couriermail.com.au paywall' do
      before do
        extractor.extracted_data = {
          "title" => "Couriermail.com.au | Subscribe to The Courier Mail for exclusive stories",
          "content" => "<body> <caas-subscription-form> </caas-subscription-form> <caas-analytics> </caas-analytics> <caas-exit-popup></caas-exit-popup>\n</body>",
          "url" => "https://www.couriermail.com.au/subscribe/news/1/"
        }
      end

      it { is_expected.to be true }
    end

    context 'when matches dailytelegraph.com.au paywall' do
      before do
        extractor.extracted_data = {
          "title" => "Subscribe to The Daily Telegraph for exclusive stories",
          "content" => "<body> <caas-subscription-form> </caas-subscription-form> <caas-analytics> </caas-analytics> <caas-exit-popup></caas-exit-popup>\n</body>",
          "url" => "https://www.dailytelegraph.com.au/subscribe/news/1/",
        }
      end

      it { is_expected.to be true }
    end

    context 'when matches bloomberg capture' do
      before do
        extractor.extracted_data = {
          "url"=>"https://www.bloomberg.com/news/articles/2019-05-24/australia-s-gas-industry-faces-climate-test-despite-election-win",
          "title"=>"Bloomberg",
          "author"=>nil,
          "domain"=>"www.bloomberg.com",
          "content"=>"<section class=\"box main\"> <p class=\"continue\">To continue, please click the box below to let us know you&apos;re not a robot.</p> </section>",
          "excerpt"=>"To continue, please click the box below to let us know you're not a robot."
        }
      end

      it 'lets it pass through for manual correction' do
        is_expected.to be false
      end
    end
  end
end
