require 'rails_helper'

RSpec.describe Document, type: :model do
  it { is_expected.to(validate_presence_of(:url)) }

  it 'validates uniqueness of url' do
    expect(Document.new(url: 'http://link.org')).to validate_uniqueness_of(:url)
  end

  it 'validates URL format of url' do
    expect(Document.new(url: 'foo')).not_to be_valid
    expect(Document.new(url: 'link.org')).not_to be_valid
    expect(Document.new(url: 'http/link.org')).not_to be_valid
    expect(Document.new(url: 'www.link.org')).not_to be_valid
    expect(Document.new(url: 'http://link.org')).to be_valid
    expect(Document.new(url: 'http://www.link.org')).to be_valid
    expect(Document.new(url: 'https://link.org')).to be_valid
    expect(Document.new(url: 'https://link.org/foo/bar')).to be_valid
  end

  describe 'before_create populate_published_at_from_extracted_data' do
    it 'successfully'do
      doc = Document.create!(
        url: 'https://link.org',
        data: {
          extracted: {
            "date_published" => "2019-08-06T17:55:34.000Z"
          }
        }
      )

      expect(doc.published_at.to_s).to eql '2019-08-06'
    end

    context 'for tweets' do
      it 'successfully' do
        doc = Document.new(
          url: 'https://link.org',
          data: {
            tweet: {
              "created_at" => "Wed Jun 05 23:11:53 +0000 2019"
            }
          }
        )
        allow(doc).to receive(:is_tweet?).and_return true

        doc.save!

        expect(doc.published_at.to_s).to eql '2019-06-05'
      end
    end
  end

  describe '#is_tweet?' do
    let(:document) { Document.new }
    subject { document.is_tweet? }

    context 'when there is no tweet id or twitter url' do
      it { is_expected.to be false }
    end

    context 'when a tweet id is present' do
      before { document.data = { initial: { 'twitter_id' => 'foo' } } }

      it { is_expected.to be true }
    end

    context 'when the URL is for a tweet' do
      before { document.url = 'https://twitter.com/BCAcomau/status/1144114837179338753' }

      it { is_expected.to be true }
    end

    context 'returns false for non tweet twitter urls' do
      before { document.url = 'https://twitter.com/BCAcomau'  }

      it { is_expected.to be false }
    end
  end

  describe 'tweet_id' do
    let(:document) { Document.new(url: 'https://link.org') }
    subject { document.tweet_id }

    context 'when it has no data' do
      it { is_expected.to eql nil }

      context 'but url for tweet' do
        before do
          document.url = 'https://twitter.com/Dhakshayini_S/status/1150525432715870209'
        end

        it { is_expected.to eql '1150525432715870209' }
      end
    end

    context 'when it has initial data with tweet_id' do
      before do
        document.data = { 'initial' => { 'twitter_id' => '1150668730537369600' } }
      end

      it { is_expected.to eql '1150668730537369600' }

      context 'and url for tweet' do
        before do
          document.url = 'https://twitter.com/Dhakshayini_S/status/1150525432715870209'
        end

        it do
          is_expected.to eql '1150668730537369600'
        end
      end
    end
  end

  describe '#title' do
    let(:document) { Document.new(url: 'https://link.org') }
    subject { document.title }

    context 'when no data is present' do
      it { is_expected.to eql nil }
    end

    context 'when no data["initial"]["title"] or data["initial"]["name"] is present' do
      before { document.data = { 'foo' => 'bar' } }

      it { is_expected.to eql nil }
    end

    context 'when data["initial"]["name"] is present' do
      before { document.data = { 'initial' => { 'name' => 'Page Name' } } }

      it { is_expected.to eql 'Page Name' }

      context 'and data["initial"]["title"] is present' do
        before { document.data = document.data.merge({ 'initial' => { 'title' => 'Page Title' } }) }

        it { is_expected.to eql 'Page Title' }
      end

      context 'and data["extracted"]["title"] is present' do
        before do
          document.data = document.data.merge(
            { 'extracted' => { 'title' => 'Extracted Title' } }
          )
        end

        it { is_expected.to eql 'Extracted Title' }
      end
    end

    context 'when data["initial"]["title"] is present' do
      before { document.data = { 'initial' => { 'title' => 'Page Title' } } }

      it { is_expected.to eql 'Page Title' }
    end
  end

  describe '#extract' do
    let(:document) { Document.new(url: 'https://link.org') }
    subject { document.extract }

    context 'when no data is present' do
      it { is_expected.to eql nil }
    end

    context 'when no data["initial"]["summary"] or data["initial"]["content"] is present' do
      before { document.data = { 'foo' => 'bar' } }

      it { is_expected.to eql nil }
    end

    context 'when data["initial"]["content"] is present' do
      before { document.data = { 'initial' => { 'content' => 'Content' } } }

      it { is_expected.to eql 'Content' }

      context 'and contains HTML' do
        before { document.data = { 'initial' => { 'content' => '<p>Content <em>marked-up</em></p>' } } }

        it { is_expected.to eql 'Content marked-up' }
      end

      context 'and is longer than 240 characters' do
        before { document.data = { 'initial' => { 'content' => 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx' } } }

        it 'is truncated to 240' do
          expect(subject.length).to eql 240
        end
      end

      context 'and data["initial"]["summary"] is present' do
        before { document.data = { 'initial' => { 'summary' => 'Summary' } } }

        it { is_expected.to eql 'Summary' }
      end
    end

    context 'when data["initial"]["summary"] is present' do
      before { document.data = { 'initial' => { 'summary' => 'Summary' } } }

      it { is_expected.to eql 'Summary' }
    end

    context 'when data["extracted"]["content"] is present' do
      before do
        document.data = {
          'extracted' => {
            'content' => '<p>Extracted content <em>marked-up</em></p>'
          },
          'initial' => {
            'summary' => 'initial summary',
            'content' => 'initial content'
          }
        }
      end

      it { is_expected.to eql 'Extracted content marked-up' }

      context 'and data["extracted"]["exerpt"] is present' do
        before do
          document.data = {
            'extracted' => {
              'content' => '<p>Extracted content <em>marked-up</em></p>',
              'exerpt' => 'Extracted exerpt'
            },
            'initial' => {
              'summary' => 'initial summary',
              'content' => 'initial content'
            }
          }
        end

        it { is_expected.to eql 'Extracted exerpt' }
      end
    end
  end

  describe 'url_host' do
    it { expect(Document.new(url: 'https://www.afr.com/news/policy/climate/explained-why-business-won-t-enlist-in-morrison-s-energy-war-on-labor-20190405-p51b17').url_host).to eq 'afr.com' }
    it { expect(Document.new(url: 'https://www.theaustralian.com.au/nation/politics/economic-reform-urgent-says-business-council/news-story/cfdb58d82c4017312280bf89dadd31b4').url_host).to eq 'theaustralian.com.au' }
    it { expect(Document.new(url: 'https://specialreports.theaustralian.com.au/1352317/').url_host).to eq 'specialreports.theaustralian.com.au' }
  end

  describe 'source' do
    it { expect(Document.new(url: 'https://www.afr.com/news/policy/climate/explained-why-business-won-t-enlist-in-morrison-s-energy-war-on-labor-20190405-p51b17').source).to eq 'Australian Financial Review' }
    it { expect(Document.new(url: 'https://www.theaustralian.com.au/nation/politics/economic-reform-urgent-says-business-council/news-story/cfdb58d82c4017312280bf89dadd31b4').source).to eq 'The Australian' }
    it { expect(Document.new(url: 'https://specialreports.theaustralian.com.au/1352317/').source).to eq 'The Australian' }
    it { expect(Document.new(url: 'https://foo.bar').source).to eq 'foo.bar' }
  end

  describe 'published_at_display' do
    it { expect(Document.new(published_at: nil).published_at_display).to eq nil }
    it { expect(Document.new(published_at: Date.new(2018,7,5)).published_at_display).to eq '5 Jul 2018' }
    it { expect(Document.new(published_at: Date.new(2019,12,30)).published_at_display).to eq '30 Dec 2019' }
  end
end
