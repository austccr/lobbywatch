require 'rails_helper'
require 'csv'

RSpec.describe Entity, type: :model do
  let(:entity) do
    Entity.new(id: 2, name: 'Mins Council', short_name: 'mca', slug: 'mca' )
  end

  it { expect(entity).to validate_uniqueness_of(:slug) }
  it { expect(entity).to validate_presence_of(:slug) }

  it { expect(entity).to validate_uniqueness_of(:name) }
  it { expect(entity).to validate_presence_of(:name) }

  it { expect(entity).to validate_uniqueness_of(:short_name).allow_blank }

  it 'does not allow non-slug values for slug' do
    is_expected.not_to allow_values('MCA', 'bar!#', 'Foo bar').for(:slug)
  end

  it 'allows slug values for slug' do
    is_expected.to allow_values('mca', 'foo-bar', '123bar').for(:slug)
  end

  describe '#short_name_or_fallback' do
    context 'when there is a short name' do
      it 'uses it' do
        entity = Entity.new(short_name: 'FB', name: 'Foo Bar')
        expect(entity.short_name_or_fallback).to eq 'FB'
      end
    end

    context 'when there is a blank short name' do
      it 'uses name' do
        entity = Entity.new(short_name: '', name: 'Foo Bar')
        expect(entity.short_name_or_fallback).to eq 'Foo Bar'
      end
    end

    context 'when there is no short name' do
      it 'uses the name' do
        entity = Entity.new(short_name: nil, name: 'Foo Bar')
        expect(entity.short_name_or_fallback).to eq 'Foo Bar'
      end
    end
  end

  describe '#years_covered_by_items' do
    let(:entity) do
      Entity.create!(name: 'Mins Council', short_name: 'mca', slug: 'mca')
    end

    subject { entity.years_covered_by_items }

    context 'when the items have no dates' do
      before do
        entity.items.create!(
          [ { published_at: nil }, { published_at: nil } ]
        )
      end

      it { is_expected.to eq [] }
    end

    context 'when one item has a dates' do
      before do
        entity.items.create!(
          [
            { published_at: '2018-12-21' },
            { published_at: nil }
          ]
        )
      end

      it { is_expected.to eq [2018] }
    end

    context 'when the items have dates' do
      before do
        entity.items.create!(
          [
            { published_at: '2018-12-21' },
            { published_at: '2019-04-01' },
            { published_at: '2016-10-15' }
          ]
        )
      end

      it 'returns an array with the year of the first and last items, and those inbetween' do
        is_expected.to eq [2016, 2017, 2018, 2019]
      end
    end
  end
end
