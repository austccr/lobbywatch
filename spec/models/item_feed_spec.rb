require 'rails_helper'

RSpec.describe ItemFeed, type: :model do
  it { is_expected.to validate_presence_of(:url) }
  it { is_expected.to validate_presence_of(:name) }

  describe '.is_tweet?' do
    let(:entry) { {} }
    subject { ItemFeed.is_tweet?(entry) }

    context 'when there is no tweet id or twitter url' do
      it { is_expected.to be false }
    end

    context 'when a tweet id is present' do
      before { entry['twitter_id'] = 'foo' }

      it { is_expected.to be true }
    end

    context 'when the URL is for a tweet' do
      before { entry['url'] = 'https://twitter.com/BCAcomau/status/1144114837179338753' }

      it { is_expected.to be true }
    end

    context 'returns false for non tweet twitter urls' do
      before { entry['url'] = 'https://twitter.com/BCAcomau' }

      it { is_expected.to be false }
    end
  end

  describe '.parse_url' do
    subject { ItemFeed.parse_url(url) }

    context 'when url is not google alert URL' do
      context do
        let(:url) { 'https://www.minerals.com.au/2019/09/business-council-shill-big-australia-will-save-economy/' }

        it 'return the original url' do
          is_expected.to eql url
        end
      end

      context do
        let(:url) { 'https://docs.google.com/spreadsheets/d/1UDBxR020TIw9FL8sdFW4qCW6iJLkUYqOR6g3kNle0A/edit#gid=0' }

        it 'return the original url' do
          is_expected.to eql url
        end
      end
    end

    context 'when url is google alert URL' do
      let(:url) { 'https://www.google.com/url?rct=j&sa=t&url=https://www.minerals.com.au/2019/09/business-council-shill-big-australia-will-save-economy/&ct=ga&cd=CAIyGmZlOWFkMfe4YWRiMWNiYjg6Y29tOmVuOlVT&usg=AFQjCNHItrigaAOqelkawZZPEZjKtOK8mA' }

      it { is_expected.to eql 'https://www.minerals.com.au/2019/09/business-council-shill-big-australia-will-save-economy/' }

      context 'and the embedded url is malformed' do
        let(:url) { 'https://www.google.com/url?rct=j&sa=t&url=https://www.abc.net.au/news/2020-03-31/aussies-reveal-their-covid-19-economic-fears-on-q+a/12104466&ct=ga&cd=CAIyGmZlOWFEF9SSsMWNiYmVuOlVT&usg=AFQjCNEriasdaasUqlowGI8InwW9WCLfw' }

        it 'return the correct URL' do
          is_expected.to eql 'https://www.abc.net.au/news/2020-03-31/aussies-reveal-their-covid-19-economic-fears-on-q%20a/12104466'
        end
      end

      context 'and the embedded url has a single value query string' do
        let(:url) { 'https://www.google.com/url?rct=j&sa=t&url=https://www.newcastleherald.com.au/story/6616762/nsw-mining-bill-ignores-climate-danger/?cs=9397&ct=ga&cd=CAIyGmE3ZjASDASDHK1231238uOlVT&usg=AFQjCNEASDASKDHGoJdilboUV8KTf8A' }

        it 'return the correct URL' do
          is_expected.to eql 'https://www.newcastleherald.com.au/story/6616762/nsw-mining-bill-ignores-climate-danger/?cs=9397'
        end
      end
    end
  end

  describe '.screen_sites' do
    let(:entries) do
      [
        {
          'url' => 'https://www.miragenews.com/loddon-shire-makes-it-easier-for-small-business/'
        },
        {
          'url' => 'https://catholicoutlook.org/i-was-at-the-amazon-synod-here-are-its-3-significant-lessons-and-challenges-for-the-church/'
        },
        {
          'url' => 'https://www.lexology.com/library/detail.aspx?g=fc8f508b-2ca3-4d51-a4d7-f9a0e120feed'
        },
        {
          'url' => 'https://www.bca.com.au/rebuilding_stronger_australia'
        },
        {
          'type' => 'passes through'
        }
      ]
    end

    it 'rejects entries that are listed for exclusion' do
      expected_entries = [
        {
          'url' => 'https://www.bca.com.au/rebuilding_stronger_australia'
        },
        {
          'type' => 'passes through'
        }
      ]

      results = VCR.use_cassette('google_sheets', record: :once) do
        ItemFeed.screen_sites(entries)
      end

      expect(results).to eq expected_entries
    end
  end

  describe '.reject_retweets' do
    let(:entries) do
      [
        {
          "title" => "retweet",
          "content" => "    <div class=\"feedbin--wrap\">\n        <p class=\"feedbin--content-text\"><a class=\"tweet-url hashtag\" href=\"https://twitter.com/search?q=%23DYK\" rel=\"nofollow\" title=\"#DYK\">#DYK</a> that gold in a nanoparticulate form has considerable potential in a range of applications that can help reduce GHG emissions? Find out more: <a href=\"https://t.co/V17W07MbYT\" rel=\"nofollow\" title=\"http://spr.ly/6017EYoyd\"><span class=\"tco-ellipsis\"><span style=\"position:absolute;left:-9999px;\"> </span></span><span style=\"position:absolute;left:-9999px;\">http://</span><span class=\"js-display-url\">spr.ly/6017EYoyd</span><span style=\"position:absolute;left:-9999px;\"></span><span class=\"tco-ellipsis\"><span style=\"position:absolute;left:-9999px;\"> </span></span></a></p>\n            <div class=\"feedbin--meta-wrap\">\n                <p class=\"feedbin--meta\">\n                    <a href=\"https://twitter.com/MineralsCouncil\">\n                        <small>Retweeted by MineralsCouncilAust</small>\n                    </a>\n                </p>\n            </div>\n",
          "twitter_id" => 'foo'
        },
        {
          "title" => "another retweet",
          "content" => "    <div class=\"feedbin--wrap\">\n        <p class=\"feedbin--content-text\"><a class=\"tweet-url hashtag\" href=\"https://twitter.com/search?q=%23natgas\" rel=\"nofollow\" title=\"#natgas\">#natgas</a> can complement variable <a class=\"tweet-url hashtag\" href=\"https://twitter.com/search?q=%23renewable\" rel=\"nofollow\" title=\"#renewable\">#renewable</a> electricity generation</p>\n            <div class=\"feedbin--meta-wrap\">\n                <p class=\"feedbin--meta\">\n                    <a href=\"https://twitter.com/sententia_gas\">\n                        <small>Retweeted by Andrew McConville</small>\n                    </a>\n                </p>\n            </div>\n            <p class=\"feedbin--media-wrap\">\n                    <a href=\"https://pbs.twimg.com/media/D93WIajX4AIAeq9.jpg:large\">\n                        <img src=\"https://pbs.twimg.com/media/D93WIajX4AIAeq9.jpg:large\" alt=\"D93wiajx4aiaeq9\">\n</a>            </p>\n    </div>\n",
          "twitter_id" => 'foo'
        },
        {
          "title" => "original tweet",
          "content" => "    <div class=\"feedbin--wrap\">\n        <p class=\"feedbin--content-text\">Australia’s Parkes Observatory radio telescope was used by NASA during the Apollo 11 Moon landing. <a class=\"tweet-url hashtag\" href=\"https://twitter.com/search?q=%2330Things\" rel=\"nofollow\" title=\"#30Things\">#30Things</a></p>\n            <p class=\"feedbin--media-wrap\">\n                    <a href=\"https://pbs.twimg.com/media/D7NVKIOXkAU1SSf.jpg:large\">\n                        <img src=\"https://pbs.twimg.com/media/D7NVKIOXkAU1SSf.jpg:large\" alt=\"D7nvkioxkau1ssf\">\n</a>            </p>\n    </div>\n",
          "twitter_id" => 'foo'
        },
        {
          "title" => "non-tweet entry",
          "content" => "<p>Data Retweeted today from the Australian Bureau of Statistics shows that record profits from Australia’s minerals sector are making Australia’s economy stronger.</p>\n<p>During 2018, pre-tax profits of resource companies increased to a record $81 billion.</p>\n<p>Mining makes up 33 per cent of all company profits in Australia, which makes it a significant contributor to government revenues which fund teachers, doctors, police and other essential services and infrastructure.</p>\n<p>With resource company profits growing by $16.9 billion during 2018, it is clear that strong prices for iron ore and coal will support a faster return to Budget surplus.</p>\n<p>A stronger mining industry is good for Australia.</p>\n<p>ends</p>\n<p> </p>\n",
        }
      ]
    end

    it 'rejects entries that are retweets' do
      parsed_entries = ItemFeed.reject_retweets(entries)
      expect(parsed_entries.map {|e| e["title"] }).to eq ["original tweet", "non-tweet entry"]
    end
  end

  describe '#fetch_items' do
    before do
      Typhoeus.stub(ENV['EXCLUDED_SITES_LIST_URL']).and_return(
        Typhoeus::Response.new(
          code: '200',
          body: File.read('spec/fixtures/google_sheet_urls_example.json')
        )
      )
    end

    let(:feed) do
      ItemFeed.new(
        id: 3,
        name: 'MCA media releases morph.io scraper',
        url: 'https://api.morph.io/austccr/mca_media_releases_scraper/data.json?key=secret&query=select%20*%20from%20%22data%22%20limit%2010'
      )
    end

    context 'with an entity' do
      before { feed.entity_id = 5 }

      context 'when there are no items' do
        before do
          Typhoeus.stub(feed.url).and_return(
            Typhoeus::Response.new(
              code: '200',
              body: "[]"
            )
          )
        end

        it 'returns empty' do
          expect(feed.fetch_items).to eq []
        end
      end

      context 'when there is an item' do
        before do
          Typhoeus.stub(feed.url).and_return(
            Typhoeus::Response.new(
              code: '200',
              body: File.read('spec/fixtures/morph_response_example.json')
            )
          )
        end

        it 'returns item attributes' do
          expect(feed.fetch_items).to eq [
            {
              document_attributes: {
                url: "http://minerals.org.au/news/stronger-mining-industry-supports-faster-return-budget-surplus",
                published_at: "2019-03-05 22:27:28 UTC",
                data: {
                  initial: {
                    "name" => "Stronger mining industry supports faster return to Budget surplus",
                    "url" => "http://minerals.org.au/news/stronger-mining-industry-supports-faster-return-budget-surplus",
                    "scraped_at" => "2019-03-11 15:32:01 UTC",
                    "published" => "2019-03-05 22:27:28 UTC",
                    "updated" => "2019-03-05 22:27:41 UTC",
                    "author" => "MCA National",
                    "summary" => "Data from the Australian Bureau of Statistics shows that record profits from Australia’s minerals sector are making Australia’s economy stronger. During 2018, pre-tax profits of resource companies increased to a record $81 billion. Mining makes up 33 per cent of all company profits in Australia, which makes it a significant contributor to government revenues which fund teachers, doctors, police and other essential services and infrastructure.",
                    "content" => "<p>Data from the Australian Bureau of Statistics shows that record profits from Australia’s minerals sector are making Australia’s economy stronger.</p>\n<p>During 2018, pre-tax profits of resource companies increased to a record $81 billion.</p>\n<p>Mining makes up 33 per cent of all company profits in Australia, which makes it a significant contributor to government revenues which fund teachers, doctors, police and other essential services and infrastructure.</p>\n<p>With resource company profits growing by $16.9 billion during 2018, it is clear that strong prices for iron ore and coal will support a faster return to Budget surplus.</p>\n<p>A stronger mining industry is good for Australia.</p>\n<p>ends</p>\n<p> </p>\n",
                    "syndication" => "https://web.archive.org/web/20190311153202/http://minerals.org.au/news/stronger-mining-industry-supports-faster-return-budget-surplus",
                    "org" => "Minerals Council of Australia",
                    "photo" => "http://minerals.org.au/sites/default/files/Trade%20%26%20investment_3_5.jpg"
                  }
                }
              },
              published_at: "2019-03-05 22:27:28 UTC",
              published_at_raw: "2019-03-05 22:27:28 UTC",
              entity_id: 5,
              type: nil,
              item_feed_id: 3
            },
            {
              document_attributes: {
                url: "http://minerals.org.au/news/mca-calls-parliament-support-regional-communities-and-jobs",
                published_at: "2019-02-20 02:10:20 UTC",
                data: {
                  initial: {
                    "name" => "MCA calls on Parliament to support regional communities and jobs",
                    "url" => "http://minerals.org.au/news/mca-calls-parliament-support-regional-communities-and-jobs",
                    "scraped_at" => "2019-03-11 15:32:19 UTC",
                    "published" => "2019-02-20 02:10:20 UTC",
                    "updated" => "2019-02-20 02:11:44 UTC",
                    "author" => "Tania Constable, Chief Executive Officer",
                    "summary" => "Australia’s world-leading minerals sector delivers high-paying, highly skilled jobs in regional and remote Australia. It is important that our political leaders continue to support stronger regional communities and jobs. Talking down our major export earning industries undermines long-term relationships and valuable trade opportunities with Australia’s global customers.",
                    "content" => "<p>Australia’s world-leading minerals sector delivers high-paying, highly skilled jobs in regional and remote Australia.</p>\n<p>It is important that our political leaders continue to support stronger regional communities and jobs.</p>\n<p>Talking down our major export earning industries undermines long-term relationships and valuable trade opportunities with Australia’s global customers.</p>\n<p>Only this month, Australian Bureau of Statistics trade data confirmed that coal became Australia’s number one export earner in 2018, with higher prices and export volumes supporting a record high $66 billion in export revenue.</p>\n<p>Higher prices and increased volumes deliver more taxes and royalties to the Federal and State governments which fund more teachers, nurses, police and lifesaving medical treatments<strong>.</strong></p>\n<p> </p>\n<p>ends</p>\n<p></p>\n<div class=\"media media-element-container media-default\">\n<div id=\"file-3087\" class=\"file file-document file-application-pdf\">\n\n        <h2 class=\"element-invisible\"><a href=\"/files/190220-mca-calls-parliament-support-regional-communities-and-jobspdf\">190220 MCA calls on Parliament to support regional communities and jobs.pdf</a></h2>\n    \n  \n  <div class=\"content\">\n    <span class=\"file\"><img class=\"file-icon\" alt=\"PDF icon\" title=\"application/pdf\" src=\"/modules/file/icons/application-pdf.png\"><a href=\"https://minerals.org.au/sites/default/files/190220%20MCA%20calls%20on%20Parliament%20to%20support%20regional%20communities%20and%20jobs.pdf\" type=\"application/pdf; length=165137\">190220 MCA calls on Parliament to support regional communities and jobs.pdf</a></span>  </div>\n\n  \n</div>\n</div>\n<p> </p>\n",
                    "syndication" => "https://web.archive.org/web/20190311153220/http://minerals.org.au/news/mca-calls-parliament-support-regional-communities-and-jobs",
                    "org" => "Minerals Council of Australia",
                    "photo" => "http://minerals.org.au/sites/default/files/180629%20Coal2.jpg"
                  }                }
              },
              published_at: "2019-02-20 02:10:20 UTC",
              published_at_raw: "2019-02-20 02:10:20 UTC",
              entity_id: 5,
              type: nil,
              item_feed_id: 3
            },
            {
              document_attributes: {
                url: "http://minerals.org.au/news/powering-future-australian-mining-people-innovation-and-modern-education",
                published_at: "2019-02-13 20:56:57 UTC",
                data: {
                  initial: {
                    "name" => "Powering the future of Australian mining with people, innovation and modern education",
                    "url" => "http://minerals.org.au/news/powering-future-australian-mining-people-innovation-and-modern-education",
                    "scraped_at" => "2019-03-11 15:32:22 UTC",
                    "published" => "2019-02-13 20:56:57 UTC",
                    "updated" => "2019-02-13 20:57:32 UTC",
                    "author" => "Tania Constable, Chief Executive Officer",
                    "summary" => "Innovation, people and skills combined with technological advances will deliver a more globally competitive minerals sector that delivers fulfilling careers in highly paid, high-skilled jobs. Today’s release of EY’s Skills Map for the Future of Work – commissioned by MCA – provides a comprehensive examination of future skills and training and technology trends in the Australian minerals industry. Key findings by EY include:",
                    "content" => "<p>Innovation, people and skills combined with technological advances will deliver a more globally competitive minerals sector that delivers fulfilling careers in highly paid, high-skilled jobs.</p>\n<p>Today’s release of EY’s <em>Skills Map for the Future of Work</em> – commissioned by MCA – provides a comprehensive examination of future skills and training and technology trends in the Australian minerals industry.</p>",
                    "syndication" => "https://web.archive.org/web/20190311153222/http://minerals.org.au/news/powering-future-australian-mining-people-innovation-and-modern-education",
                    "org" => "Minerals Council of Australia",
                    "photo" => "http://minerals.org.au/sites/default/files/Skills_3_1.jpg"
                  }                }
              },
              published_at: "2019-02-13 20:56:57 UTC",
              published_at_raw: "2019-02-13 20:56:57 UTC",
              entity_id: 5,
              type: nil,
              item_feed_id: 3
            }
          ]
        end
      end
    end

    context 'with a feedbin API response' do
      before do
        feed.url = 'https://api.feedbin.com/v2/feeds/1602674/entries.json'

        Typhoeus.stub(feed.url).and_return(
          Typhoeus::Response.new(
            code: '200',
            body: [
              {
                "id" => 2078921138,
                "feed_id" => 1602674,
                "title" => "QRC commends QR on Mt Isa rail opening",
                "author" => "Anthony Donaghy",
                "summary" => "The post QRC commends QR on Mt Isa rail opening appeared first on Queensland Resources Council.",
                "content" => "<p>The post <a rel=\"nofollow\" href=\"https://www.qrc.org.au/media-releases/qrc-commends-qr-on-mt-isa-rail-opening/\">QRC commends QR on Mt Isa rail opening</a> appeared first on <a rel=\"nofollow\" href=\"https://www.qrc.org.au\">Queensland Resources Council</a>.</p>\n",
                "url" =>  'https://www.google.com/url?rct=j&sa=t&url=https://www.minerals.com.au/2019/09/business-council-shill-big-australia-will-save-economy/&ct=ga&cd=CAIyGmZlOWFkMfe4YWRiMWNiYjg6Y29tOmVuOlVT&usg=AFQjCNHItrigaAOqelkawZZPEZjKtOK8mA',
                "extracted_content_url" => "https://extract.feedbin.com/parser/feedbin/e689cfc7549c12e350a72e2fce4da2c3f0e2cf1c?base64_url=aHR0cHM6Ly93d3cucXJjLm9yZy5hdS9tZWRpYS1yZWxlYXNlcy9xcmMtY29tbWVuZHMtcXItb24tbXQtaXNhLXJhaWwtb3BlbmluZy8=",
                "published" => "2019-04-29T06:11:06.000000Z",
                "created_at" => "2019-04-29T14:40:55.554755Z"
              }
            ].to_json
          )
        )
      end

      it do
        expect(feed.fetch_items.pop).to eq(
          {
            published_at: '2019-04-29T06:11:06.000000Z',
            published_at_raw: '2019-04-29T06:11:06.000000Z',
            document_attributes: {
              url: 'https://www.minerals.com.au/2019/09/business-council-shill-big-australia-will-save-economy/',
              published_at: '2019-04-29T06:11:06.000000Z',
              data: {
                initial: {
                  "id" => 2078921138,
                  "feed_id" => 1602674,
                  "title" => "QRC commends QR on Mt Isa rail opening",
                  "author" => "Anthony Donaghy",
                  "summary" => "The post QRC commends QR on Mt Isa rail opening appeared first on Queensland Resources Council.",
                  "content" => "<p>The post <a rel=\"nofollow\" href=\"https://www.qrc.org.au/media-releases/qrc-commends-qr-on-mt-isa-rail-opening/\">QRC commends QR on Mt Isa rail opening</a> appeared first on <a rel=\"nofollow\" href=\"https://www.qrc.org.au\">Queensland Resources Council</a>.</p>\n",
                  "url" => "https://www.google.com/url?rct=j&sa=t&url=https://www.minerals.com.au/2019/09/business-council-shill-big-australia-will-save-economy/&ct=ga&cd=CAIyGmZlOWFkMfe4YWRiMWNiYjg6Y29tOmVuOlVT&usg=AFQjCNHItrigaAOqelkawZZPEZjKtOK8mA",
                  "extracted_content_url" => "https://extract.feedbin.com/parser/feedbin/e689cfc7549c12e350a72e2fce4da2c3f0e2cf1c?base64_url=aHR0cHM6Ly93d3cucXJjLm9yZy5hdS9tZWRpYS1yZWxlYXNlcy9xcmMtY29tbWVuZHMtcXItb24tbXQtaXNhLXJhaWwtb3BlbmluZy8=",
                  "published" => "2019-04-29T06:11:06.000000Z",
                  "created_at" => "2019-04-29T14:40:55.554755Z"
                }
              }
            },
            entity_id: nil,
            type: nil,
            item_feed_id: 3
          }
        )
      end
    end

    context 'when an entry has published_raw' do
      before do
        Typhoeus.stub(feed.url).and_return(
          Typhoeus::Response.new(
            code: '200',
            body: [
              {
                'published_raw': 'Fri, 22 Mar 2019 08:43:00 +1100',
                'published': '2019-03-21 21:43:00 UTC',
                item_feed_id: 3
              }
            ].to_json
          )
        )
      end

      it 'it uses it for published_at_raw' do
        expect(feed.fetch_items.pop.compact).to eq(
          {
            published_at: '2019-03-21 21:43:00 UTC',
            published_at_raw: 'Fri, 22 Mar 2019 08:43:00 +1100',
            item_feed_id: 3,
            document_attributes: {
              url: nil,
              published_at: '2019-03-21 21:43:00 UTC',
              data: {
                initial: {
                  'published' => '2019-03-21 21:43:00 UTC',
                  'published_raw' => 'Fri, 22 Mar 2019 08:43:00 +1100',
                  'item_feed_id' => 3
                }
              }
            }
          }
        )
      end
    end

    context 'when the feed has item_type' do
      before do
        feed.item_type = 'FEED TYPE'

        Typhoeus.stub(feed.url).and_return(
          Typhoeus::Response.new(
            code: '200',
            body: [ { "type": "ENTRY TYPE" } ].to_json
          )
        )
      end

      it 'it uses it over the entry\'s type' do
        expect(feed.fetch_items.pop.compact).to eq(
          {
            type: 'FEED TYPE',
            item_feed_id: 3,
            document_attributes: {
              url: nil,
              published_at: nil,
              data: {
                initial: {
                  'type' => 'ENTRY TYPE'
                }
              }
            }
          }
        )
      end
    end

    context 'when the feed has no item_type' do
      before do
        feed.item_type = ''
      end

      context 'and the entry has a type' do
        before do
          Typhoeus.stub(feed.url).and_return(
            Typhoeus::Response.new(
              code: '200',
              body: [ { "type": "ENTRY TYPE" } ].to_json
            )
          )
        end

        it 'uses it' do
          expect(feed.fetch_items.pop.compact).to eq(
            {
              type: 'ENTRY TYPE',
              item_feed_id: 3,
              document_attributes: {
                url: nil,
                published_at: nil,
                data: {
                  initial: {
                    'type' => 'ENTRY TYPE',
                  }
                }
              }
            }
          )
        end
      end

      context 'and the entry has no type' do
        before do
          Typhoeus.stub(feed.url).and_return(
            Typhoeus::Response.new(
              code: '200',
              body: [ { "url": "https://foo.bar" } ].to_json
            )
          )
        end

        it 'sets no type' do
          expect(feed.fetch_items.pop.compact).to eq(
            {
              document_attributes: {
                url: "https://foo.bar",
                published_at: nil,
                data: {
                  initial: {
                    'url' => "https://foo.bar"
                  }
                }
              },
              item_feed_id: 3
            }
          )
        end
      end
    end
  end
end
