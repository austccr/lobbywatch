# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ItemSearcher, type: :model do
  describe '#processed_filters' do
    it 'drops :q' do
      searcher = ItemSearcher.new(filters: { q: 'zing zang' })

      expect(searcher.processed_filters).not_to include({ q: 'zing zang' })
    end

    it 'make :year an integer' do
      searcher = ItemSearcher.new(filters: { year: '2007' })

      expect(searcher.processed_filters[:year]).to be_a Integer
    end

    it 'gets the id for the Tag' do
      Gutentag::Tag.create(id: 3, name: 'foo bar')

      searcher = ItemSearcher.new(filters: { tag: 'foo bar' })

      expect(searcher.processed_filters).to eql({ tag_ids: 3 })
    end

    it do
      Gutentag::Tag.create(id: 3, name: 'foo bar')

      searcher = ItemSearcher.new(
        filters: {
          tag: 'foo bar',
          type: 'Media releases',
          year: '2018',
          q: 'zing zang',
          entity_id: 3
        }
      )

      expect(searcher.processed_filters).to eql(
        {
          tag_ids: 3,
          year: 2018,
          type: 'Media releases',
          entity_id: 3
        }
      )
    end

    context 'with_ia_items' do
      let(:company) do
        Company.create(id: 5, name: 'BHP', slug: 'bhp')
      end

      before do
        company.memberships.create(
          [
            {
              industry_association: IndustryAssociation.create!(
                id: 7, name: 'foo', slug: 'bar'
              )
            },
            {
              industry_association: IndustryAssociation.create!(
                id: 2, name: 'zip', slug: 'zang'
              )
            }
          ]
        )
      end

      context 'is false by default' do
        it "does not add ids for entiti_id entity's industry associations" do
          searcher = ItemSearcher.new(filters: { entity_id: 5 })

          expect(searcher.processed_filters).to eql({ entity_id: 5 })
        end
      end

      context 'when with_ia_items is true' do
        it "adds to ids for entiti_id entity's industry associations" do
          searcher = ItemSearcher.new(
            filters: { entity_id: 5 },
            with_ia_items: true
          )

          expect(searcher.processed_filters).to eql({ entity_id: [5,7,2] })
        end
      end
    end
  end
end
