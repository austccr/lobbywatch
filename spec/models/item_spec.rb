require 'rails_helper'

RSpec.describe Item, type: :model do
  it 'can have no entity' do
    expect(Item.new(entity:nil)).to be_valid
  end

  it 'validates uniquess of document_id' do
    expected_message = 'URL has already been collected for this lobby group'
    item = Item.new(
      document: Document.create(url: 'https://link.org')
    )

    expect(item).to(
      validate_uniqueness_of(:document_id).scoped_to(:entity_id).with_message(expected_message)
    )
  end

  it 'allows multiple Items with no document' do
    Item.create(document: nil)
    item = Item.new(document: nil)

    expect(item).to be_valid
  end

  it 'associates to existing document if it exists' do
    Document.create!(id: 5, url: 'https://foo.bar')

    item = Item.create!(
      document_attributes: { url: 'https://foo.bar' }
    )

    expect(item.document_id).to eq 5
  end

  context 'when passed blank document_attributes' do
    it 'does not create a Document' do
      Item.create!(
        document_attributes: { url: '' }
      )

      expect(Document.count).to eq 0
    end
  end

  it 'creates a Document' do
    Item.create!(
      document_attributes: { url: 'https://foo.bar' }
    )

    expect(Document.first.url).to eq 'https://foo.bar'
  end

  context 'when passed a new document url' do
    let(:item) do
      Item.create!(
        document: Document.create!(id: 5, url: 'https://foo.bar')
      )
    end

    context 'for a new document' do
      before do
        item.update!(
          document_attributes: { url: 'https://wiz.zap' },
        )
      end

      it 'does not update the existing document' do
        expect(Document.find(5).url).to eq 'https://foo.bar'
      end

      it 'creates and associates to a new document' do
        expect(item.document.url).to eq 'https://wiz.zap'
        expect(item.document_id).not_to eq 5
      end
    end

    context 'for an existing document' do
      before do
        Document.create!(id: 7, url: 'https://klang.boop')
      end

      it 'is associated with it' do
        item.update!(
          document_attributes: { url: 'https://klang.boop' },
        )

        expect(item.document_id).to eq 7
      end
    end
  end

  it 'Can update non-URL attrbiutes of associated document' do
    document = Document.create!(id: 5, url: 'https://same.url', published_at: nil)
    item = Item.create!(document: document)

    item.update!(
      document_attributes: {
        id: 5,
        published_at: '2011-01-01',
        url: 'https://same.url',
      }
    )

    expect(document.published_at.to_s).to eq '2011-01-01'
  end

  describe '#summary_or_tweet_text' do
    let(:item) { Item.new(type: nil) }

    context 'when the item is not a tweet' do
      context 'with a summary' do
        before { item.summary = 'summary' }

        it 'is the summary' do
          expect(item.summary_or_tweet_text).to eql 'summary'
        end
      end

      context 'without a summary' do
        before { item.summary = '' }

        it 'is nil' do
          expect(item.summary_or_tweet_text).to eql nil
        end
      end
    end

    context 'for a tweet' do
      let(:document) do
        Document.create!(id: 5, url: 'https://same.url', published_at: nil)
      end

      before do
        allow(document).to receive(:is_tweet?).and_return(true)
        allow(document).to receive(:full_text).and_return('tweet text')

        item.document = document
      end

      context 'with a summary' do
        before { item.summary = 'summary' }

        it 'is the summary' do
          expect(item.summary_or_tweet_text).to eql 'summary'
        end
      end

      context 'without a summary' do
        before { item.summary = '' }

        it 'is the tweet text' do
          expect(item.summary_or_tweet_text).to eql '"tweet text"'
        end
      end
    end
  end

  describe '#parse_and_set_type' do
    let(:item) { Item.new(type: nil) }

    subject { item.type }

    it do
      item.parse_and_set_type(nil)

      is_expected.to eq nil
    end

    it do
      item.parse_and_set_type('')
      is_expected.to eq nil
    end

    it do
      item.parse_and_set_type('T News')
      is_expected.to eq nil
    end

    it do
      item.parse_and_set_type('Sky News')
      is_expected.to eq nil
    end

    it do
      item.parse_and_set_type('Renew Economy')
      is_expected.to eq nil
    end

    it do
      item.parse_and_set_type('Energy News Bulletin')
      is_expected.to eq nil
    end

    it do
      item.parse_and_set_type('Media release')
      is_expected.to eq 'Media release'
    end

    it do
      item.parse_and_set_type('submission')
      is_expected.to eq 'Submission'
    end

    it do
      item.parse_and_set_type('Submission')
      is_expected.to eq 'Submission'
    end

    it do
      item.parse_and_set_type('Advertisement')
      is_expected.to eq 'Advertisement'
    end

    it do
      item.parse_and_set_type('Speech')
      is_expected.to eq 'Speech'
    end
  end

  describe '#assign_to_duplicate_staging_items' do
    context 'when there is a staging item with the same URL' do
      let!(:document) { Document.create(id: 5, url: 'https://link.org') }
      let!(:staging_item) { StagingItem.create(document_id: 5) }
      let!(:item) { Item.create(id: 3, document_id: 5) }
      let!(:entity) { Entity.create(id: 5,slug: 'foo', name: 'Foo') }

      context 'and the same entity' do
        before do
          staging_item.update(entity_id: 5)
          item.entity_id = 5
        end

        context 'and is not pending review' do
          before { allow(staging_item).to receive(:pending_review?).and_return false }

          it 'it does not update it' do
            item.assign_to_duplicate_staging_items

            expect(staging_item.item_id).to eql nil
          end
        end

        context 'and is pending review' do
          before { allow(staging_item).to receive(:pending_review?).and_return true }

          it 'updates it item_id to the item' do
            item.assign_to_duplicate_staging_items

            staging_item.reload

            expect(staging_item.item_id).to eql 3
          end
        end
      end

      context 'for a different entity' do
        before do
          staging_item.update(entity_id: 5)
          item.entity_id = 7
        end

        it 'it does not update it' do
          item.assign_to_duplicate_staging_items

          expect(staging_item.item_id).to eql nil
        end
      end
    end
  end
end
