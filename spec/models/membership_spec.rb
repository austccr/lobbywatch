require 'rails_helper'

RSpec.describe Membership, type: :model do
  it 'does not allow duplicate memberships' do
    membership = Company.create(name: 'Foo', slug: 'foo').memberships.create(
      {
        industry_association: IndustryAssociation.create(
          name: 'Bar', slug: 'bar'
        )
      }
    )

    expect(membership).to validate_uniqueness_of(:company_id)
      .scoped_to(:industry_association_id)
  end
end
