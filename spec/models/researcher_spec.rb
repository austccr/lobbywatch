require 'rails_helper'

RSpec.describe Researcher, type: :model do
  it 'must have a long password' do
    is_expected.to validate_length_of(:password).is_at_least(15)
  end
end
