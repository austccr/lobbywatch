require 'rails_helper'

RSpec.describe StagingItem, type: :model do
  describe 'validates document_id against Item' do
    let(:entity) { Entity.create!(name: 'foo', slug: 'foo') }
    let(:existing_document) { Document.create!(url: 'http://item.org') }

    before do
      Item.create!(document: existing_document, entity: entity)
    end

    context 'when an item with a different document exists' do
      let(:new_url) do
        StagingItem.new(
          document_attributes: { url: 'http://new_item.org' },
          entity: entity
        )
      end

      it 'is valid' do
        expect(new_url).to be_valid
      end
    end

    context 'when an item with the same url and entity exists' do
      let(:existing_url) do
        StagingItem.new(
          document_attributes: { url: existing_document.url },
          entity: entity
        )
      end

      it 'is invalid' do
        expect(existing_url).to be_invalid
      end
    end

    context 'when an item with the same url and different entity exists' do
      let(:existing_url_different_entity) do
        StagingItem.new(
          document_attributes: { url: existing_document.url },
          entity: Entity.create!(name: 'bar', slug: 'bar')
        )
      end

      it 'is valid' do
        expect(existing_url_different_entity).to be_valid
      end
    end
  end

  describe '#pending_review?' do
    let(:staging_item) { StagingItem.new }
    subject { staging_item.pending_review? }

    context 'when it has an item' do
      before { allow(staging_item).to receive(:has_item).and_return true }

      it { is_expected.to eq false }
    end

    context 'when it is dismissed' do
      before { staging_item.dismissed = true }

      it { is_expected.to eq false }
    end

    context 'when it has no item and is not dismissed' do
      before do
        staging_item.dismissed = false
        allow(staging_item).to receive(:has_item).and_return false
      end

      it { is_expected.to eq true }
    end
  end
end
