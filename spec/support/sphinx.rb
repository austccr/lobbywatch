RSpec.configure do |config|
  config.before(:suite) do
    # Configure and start Sphinx for system specs
    ThinkingSphinx::Test.init
    ThinkingSphinx::Test.start index: false
  end

  config.before(:each, sphinx: :true) do |example|
    ThinkingSphinx::Configuration.instance.settings['real_time_callbacks'] =
      (example.metadata[:sphinx] == :true)
  end

  config.after(:all, sphinx: :true) do
    ThinkingSphinx::Test.clear
  end

  config.after(:suite) do
    ThinkingSphinx::Test.stop
  end
end
