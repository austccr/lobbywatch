require 'rails_helper'

RSpec.describe "ResearcherAddsItemFeed", type: :system do
  describe "Researcher adds a new item feed" do
    it "successfully" do
      # For now, researchers add feeds on the command line
      ItemFeed.create(
        name: "AFGC News RSS (FeedBin)",
        url: "https://api.feedbin.com/v2/feeds/1602689/entries.json",
        entity: Entity.create(slug: 'afgc', name: 'Australian Food and Grocery Council'),
        item_type: "Media release",
        info_url: "https://www.afgc.org.au/category/news/"
      )

      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678')

      visit 'staging/feeds'

      expect(page).to have_content 'AFGC News RSS (FeedBin)'
    end
  end
end

