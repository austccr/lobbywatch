require 'rails_helper'

RSpec.describe "ResearcherAddsDescriptionToEntity", type: :system, sphinx: :true do
  describe "Researcher adds a description to an entity" do
    before do
      IndustryAssociation.create(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'industry_associations/mca'

      click_link 'Add a description'

      fill_in 'Add a description', with: 'The Minerals Council of Australia (MCA) are *aggressively* pro-coal.'

      click_button 'Submit'

      expect(page.body).to include 'The Minerals Council of Australia (MCA) are <em>aggressively</em> pro-coal.'
    end
  end
end
