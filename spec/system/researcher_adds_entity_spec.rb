require 'rails_helper'

RSpec.describe "ResearcherAddsEntity", type: :system, sphinx: :true do
  describe "Researcher adds a new Industry association" do
    it "successfully", driver: :rack_test do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit root_path

      click_link 'Add a new entity to track'

      fill_in 'Full name', with: 'Australian Food and Grocery Council'
      fill_in 'Short name', with: 'AFGC'
      fill_in 'Slug', with: 'afgc'
      select 'Industry association', from: 'What type of entity is this?'
      fill_in 'Add a description', with: 'Members include: Coca Cola Amatil'

      click_button 'Submit'

      visit root_path

      expect(page).to have_content 'Industry associations'
      expect(page).to have_content 'Australian Food and Grocery Council'
    end
  end

  describe "Researcher adds a new Company" do
    it "successfully", driver: :rack_test do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit root_path

      click_link 'Add a new entity to track'

      fill_in 'Full name', with: 'Woodside Petroleum'
      fill_in 'Short name', with: 'Woodside'
      fill_in 'Slug', with: 'woodside-petroleum'
      select 'Company', from: 'What type of entity is this?'

      click_button 'Submit'

      visit root_path

      # Not promoted
      expect(page).not_to have_content 'Woodside Petroleum'

      visit '/companies'

      expect(page).to have_content 'Woodside Petroleum'
    end
  end
end
