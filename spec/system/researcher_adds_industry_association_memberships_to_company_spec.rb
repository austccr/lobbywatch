require 'rails_helper'

RSpec.describe "ResearcherAddsIndustryAssociationMembershipsToCompany", type: :system do
  describe "Researcher adds an industry association membership to a company" do
    before do
      Company.create!(name: 'BHP', slug: 'bhp')
      IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca')
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'companies/bhp/memberships'

      click_link 'Add a membership'

      select 'Minerals Council of Australia', from: 'Industry association'

      click_button 'Submit'

      expect(page.text).to include 'BHP is a member of the Minerals Council of Australia.'
    end
  end
end

