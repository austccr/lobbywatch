require 'rails_helper'

RSpec.describe "ResearcherAddsItemFromStaging", type: :system do
  describe "Researcher adds a new item from staging" do
    let(:document) do
      Document.create!(
        id: 5,
        url: 'https://www.qrc.org.au/media-releases/qrc-welcomes-future-skills-partnership-between-bma-tafe-and-cqu/',
        published_at: Time.new(2019, 4, 5),
        data: {
          extracted: {
            title: 'QRC welcomes Future Skills Partnership between BMA, TAFE and CQU - Queensland Resources Council',
            exerpt: 'The Queensland Resources Council has welcomed the commitment of the Queensland Futures Skills Partnership between its member company BHP Mitsubishi Alliance (BMA), TAFE Queensland and Central Queensland University (CQU). QRC Chief Execu...',
          }
        }
      )
    end

    before do
      IndustryAssociation.create!(
        name: 'Minerals Council of Australia',
        short_name: 'mca', slug: 'mca'
      )

      StagingItem.create!(
        id: 3,
        document: document,
        type: 'Media release',
        entity: Entity.find_by_slug('mca')
      )
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit '/staging'

      within '#item-3' do
        click_on 'Add item'
      end

      # Preview is displayed on form page
      expect(page).to have_content 'QRC welcomes Future Skills Partnership between BMA, TAFE and CQU - Queensland Resources Council'
      expect(page).to have_content 'The Queensland Resources Council has welcomed the commitment of the Queensland Futures Skills Partnership between its member company BHP Mitsubishi Alliance (BMA), TAFE Queensland and Central Queensland University (CQU). QRC Chief Execu...'
      expect(page).to have_content '5 Apr 2019'

      fill_in 'Summary', with: 'Measured response vital in reducing Australia’s emissions without damaging economy & jobs'
      fill_in 'Tags', with: 'Employement, Tania Constable'

      click_button 'Submit'
      expect(page).to have_content 'Successfully added a new item'

      click_link 'a new item'

      expect(page).to have_content 'Media release'
      expect(page).to have_content '5 Apr 2019'
      expect(page).to have_content 'Measured response vital in reducing Australia’s emissions without damaging economy & jobs'
      expect(page).to have_link href: 'https://www.qrc.org.au/media-releases/qrc-welcomes-future-skills-partnership-between-bma-tafe-and-cqu/'

      visit '/staging'

      # check that the staging item has been hidden
      expect(page).not_to have_content 'This is the summary of the document'
    end
  end
end

