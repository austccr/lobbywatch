require 'rails_helper'

RSpec.describe "ResearcherAddsItem", type: :system, sphinx: :true do
  describe "Researcher adds a new item" do
    let!(:entity) { IndustryAssociation.create(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca') }

    before :each do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)
    end

    it "successfully" do
      visit 'items/new'

      select 'Minerals Council of Australia', from: 'Who\'s lobbying here?'

      fill_in 'URL', with: 'https://www.minerals.org.au/news/measured-response-vital-reducing-australia%E2%80%99s-emissions-without-damaging-economy-jobs'
      fill_in 'Date published', with: '2018-12-21'
      fill_in 'Summary', with: 'Measured response vital in reducing Australia’s emissions without damaging economy & jobs'
      fill_in 'Type', with: 'Media release'
      fill_in 'Tags', with: 'Employement, Tania Constable'

      click_button 'Submit'

      expect(page).to have_content 'Successfully added a new item. Nice work :)'

      within 'h1' do
        expect(page).to have_content 'Minerals Council of Australia'
      end

      within '.item' do
        expect(page).to have_content 'Measured response vital in reducing Australia’s emissions without damaging economy & jobs'
        expect(page).to have_content 'minerals.org.au'
        expect(page).to have_content '21 Dec 2018'
        expect(page).to have_content 'Media release'
        expect(page).to have_content 'Employement'
        expect(page).to have_content 'Tania Constable'
      end
    end

    it 'unsuccessfully because it already exists for entity' do
      Item.create!(
        entity: entity,
        document_attributes: { url: 'https://www.minerals.org.au/news/foo' }
      )

      visit 'items/new'

      select 'Minerals Council of Australia', from: 'Who\'s lobbying here?'
      fill_in 'URL', with: 'https://www.minerals.org.au/news/foo'

      click_button 'Submit'

      expect(page).to have_content 'Document URL has already been collected for this lobby group'
      expect(page).to have_content 'See the existing item that references this URL'
    end

    context 'with a url in params' do
      context 'when we already have an item with this document for the entity' do
        before do
          Item.create!(
            entity: entity,
            document_attributes: {
              url: 'https://www.minerals.org.au/news/foo',
              data: { extracted: { title: 'Existing document title' } }
            }
          )
        end

        it 'redirects to edit' do
          visit 'items/new?url=https://www.minerals.org.au/news/foo&entity&entity=mca'

          expect(page).to have_content 'Edit'
          expect(page).to have_content 'Existing document title'
        end
      end
    end
  end
end
