require 'rails_helper'

RSpec.describe "ResearcherAddsMembershipToIndustryAssociation", type: :system do
  describe "Researcher adds a membership to an industry association" do
    before do
      IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca', short_name: 'MCA')
      Company.create!(name: 'BHP', slug: 'bhp')
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'industry_associations/mca/members'

      click_link 'Add a membership'

      select 'BHP', from: 'Company'

      click_button 'Submit'

      expect(page.text).to include "MCA’s members include BHP."
    end
  end
end

