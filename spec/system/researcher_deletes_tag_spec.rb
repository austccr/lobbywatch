require 'rails_helper'

RSpec.describe "ResearcherDeletesTag", type: :system do
  describe "Researcher deletes tag" do
    it 'successfully' do
      Item.create!(
        document_attributes: { url: 'https://taggeditem.org' },
        tag_names: ['Tag to Delete']
      )

      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'tags/Tag+to+Delete'

      click_link 'Edit tag'

      click_button 'Delete tag'

      expect(page).to have_content 'Successfully deleted tag'
      expect(find('.indexPage')).not_to have_content 'Tag to Delete'
    end
  end
end

