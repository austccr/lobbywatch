require 'rails_helper'

RSpec.describe "ResearcherDownloadsItemsCSV", type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher downloads items CSV for entity" do
      before do
        entity = IndustryAssociation.create(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
        entity.items.create!(
          [
            {
              id: 3,
              document_attributes: {
                url: 'https://www.minerals.org.au/news/measured-response-vital-reducing-australia%E2%80%99s-emissions-without-damaging-economy-jobs',
                published_at: '2018-12-21'
              },
              tag_names: ['Energy', 'foo bar'],
              type: 'Media release'
            },
            {
              id: 7,
              document_attributes: {
                url: 'https://twitter.com/SkyNewsAust/status/1087518201007632384',
                published_at: '2019-03-01'
              },
              summary: 'This is a nice summary',
              source: 'Sky News',
              type: nil
            },
            {
              id: 9,
              document_attributes: {
                url: 'https://www.worldcoal.com/mining/03122018/mca-carmichael-coal-mine-development-to-benefit-queensland-and-australian-economies/',
                published_at: '2016-10-15'
              },
              summary: 'Another nice summary',
              source: 'World Coal',
              tag_names: ['foo bar'],
              type: 'Submission'
            }
          ]
        )
      end

      it "successfully", driver: :rack_test do
        visit 'industry_associations/mca'

        click_link 'Download as spreadsheet'

        expect(page.source).to eq <<~CSV
        id,entity,published_at,type,summary,url,source,tag_names
        7,Minerals Council of Australia,2019-03-01,,This is a nice summary,https://twitter.com/SkyNewsAust/status/1087518201007632384,Twitter,
        3,Minerals Council of Australia,2018-12-21,Media release,,https://www.minerals.org.au/news/measured-response-vital-reducing-australia%E2%80%99s-emissions-without-damaging-economy-jobs,MCA Website,"Energy, foo bar"
        9,Minerals Council of Australia,2016-10-15,Submission,Another nice summary,https://www.worldcoal.com/mining/03122018/mca-carmichael-coal-mine-development-to-benefit-queensland-and-australian-economies/,World Coal,foo bar
        CSV
      end
    end
  end
end
