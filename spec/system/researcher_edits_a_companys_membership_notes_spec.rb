require 'rails_helper'

RSpec.describe "ResearcherEditsMembershipNotes", type: :system do
  describe "Researcher edits the notes on a company's membership" do
    before do
      bhp = Company.create!(name: 'BHP', slug: 'bhp')

      bhp.memberships.create([
        { industry_association: IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca') }
      ])
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'companies/bhp/memberships'

      expect(page).to have_content 'Minerals Council of Australia'

      click_link 'Edit notes'

      fill_in "Add a note about BHP's membership to Minerals Council of Australia", with: 'A note with [a markdown link](https://accr.org.au).'

      click_button 'Submit'

      expect(page).to have_content "BHP Industry Association Memberships"
      expect(page).to have_content 'A note with a markdown link.'
    end
  end
end

