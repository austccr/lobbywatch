require 'rails_helper'

RSpec.describe "ResearcherEditsIndustryAssociationsMembershipNotes", type: :system do
  describe "Researcher edits the notes on a industry association's membership" do
    before do
      mca = IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca')

      mca.memberships.create([
        { company: Company.create!(name: 'BHP', slug: 'bhp') }
      ])
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'industry_associations/mca/members'

      expect(page).to have_content 'BHP'

      click_link 'Edit notes'

      fill_in "Add a note about BHP's membership to Minerals Council of Australia", with: 'A note with [a markdown link](https://accr.org.au).'

      click_button 'Submit'

      expect(page).to have_content 'Minerals Council of Australia Members'
      expect(page).to have_content 'A note with a markdown link.'
    end
  end
end

