require 'rails_helper'

RSpec.describe "ResearcherEditsItem", type: :system, sphinx: :true do
  describe "Researcher edits an existing item" do
    before do
      entity = IndustryAssociation.create(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
      entity.items.create!(
        id: 3,
        document_attributes: {
          url: 'https://www.minerals.org.au/news/measured-response-vital-reducing-australia%E2%80%99s-emissions-without-damaging-economy-jobs',
          published_at: nil
        },
        tag_names: ['Employement']
      )
      StagingItem.create(item: Item.first, document: Document.first)
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'industry_associations/mca'

      within '#item-3' do
        click_link 'Edit'
      end

      fill_in 'Summary', with: 'Measured response vital in reducing Australia’s emissions without damaging economy & jobs'
      fill_in 'Tags', currently_with: 'Employement',  with: 'Employement, Tania Constable'
      fill_in 'Tags', with: 'Employement, Tania Constable'
      fill_in 'Type', with: 'Media release'
      fill_in 'Date published', with: '2019-05-05'

      click_button 'Submit'

      expect(page).to have_content 'Successfully updated.'

      expect(page).to have_content 'Measured response vital in reducing Australia’s emissions without damaging economy & jobs'
      expect(page).to have_content 'minerals.org.au'
      expect(page).to have_content '5 May 2019'
      expect(page).to have_content 'Media release'
      expect(page).to have_content 'Employement'
      expect(page).to have_content 'Tania Constable'
    end
  end
end
