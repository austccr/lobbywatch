require 'rails_helper'

RSpec.describe "ResearcherEditsTag", type: :system do
  describe "Researcher edits tag" do
    it 'successfully' do
      Item.create!(
        document_attributes: { url: 'https://taggeditem.org' },
        tag_names: ['Old Tag Name']
      )

      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'tags/Old+Tag+Name'

      click_link 'Edit tag'

      fill_in 'Tag name', with: 'New Tag Name'
      fill_in 'Description', with: 'A description with [a markdown link](https://accr.org.au).'

      click_button 'Submit'

      expect(page).to have_content 'Lobbying tagged New Tag Name'
      expect(page).to have_content 'A description with a markdown link.'
      expect(page).to have_content 'taggeditem.org'
    end
  end
end

