require 'rails_helper'

RSpec.describe 'ResearcherFiltersItemsByType', type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe 'Researcher filters items by type' do
      before do
        entity = IndustryAssociation.create!(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
        entity.items.create!(
          [
            {
              id: 1,
              document_attributes: {
                url: 'https://link1.org/news/1',
                published_at: '2018-12-21',
                data: { initial: { content: 'coal' } }
              },
              type: 'media release'
            },
            {
              id: 2,
              document_attributes: {
                url: 'https://twitter.com/MineralsCouncil/status/1112556840733175809',
                published_at: '2019-04-01'
              },
              summary: 'coal',
              type: 'tweet'
            },
            {
              id: 3,
              document_attributes: {
                url: 'https://link3.org/news/3',
                published_at: '2016-10-15'
              },
              type: 'media release'
            }
          ]
        )
      end

      it 'successfully' do
        visit 'industry_associations/mca'

        fill_in 'Filter by text', with: 'coal'

        click_button 'Filter'

        expect(page.text).to include '2 items'
        expect(page.text).to include 'link1.org'
        expect(page.text).to include 'twitter.com'
        expect(page.text).not_to include 'link3.org'
      end

      context 'from the site header' do
        it 'successfully' do
          visit '/'

          fill_in 'Search collection', with: 'coal'

          click_button 'Search'

          expect(page.text).to include '2 items'
          expect(page.text).to include 'link1.org'
          expect(page.text).to include 'twitter.com'
          expect(page.text).not_to include 'link3.org'
        end
      end
    end
  end
end
