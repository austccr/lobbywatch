require 'rails_helper'

RSpec.describe "ResearcherFiltersItemsByTag", type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher filters items with a specific tag for an entity" do
      before do
        entity = IndustryAssociation.create!(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
        entity.items.create!(
          [
            {
              id: 1,
              document_attributes: {
                url: 'https://link1.org',
                published_at: '2018-12-21'
              },
              tag_names: ['foo bar', 'Energy', 'Oil & Gas']
            },
            {
              id: 2,
              document_attributes: {
                url: 'https://link2.org',
              published_at: '2019-03-01'
              },
              tag_names: ['foo bar', 'Energy']
            },
            {
              id: 3,
              document_attributes: {
                url: 'https://link3.org',
                published_at: '2016-10-15'
              },
              tag_names: ['#favs', 'Tania Constable']
            }
          ]
        )
      end

      context 'via the tag on an item' do
        it "successfully" do
          visit 'industry_associations/mca'

          within '#item-2' do
            click_link 'foo bar'
          end

          expect(page.text).to include '2 items'
          expect(page.text).to include 'Lobbying tagged foo bar'
          expect(page.text).to include 'link1.org'
          expect(page.text).to include  'link2.org'
          expect(page.text).not_to include  'link3.org'
        end
      end

      context 'via the filter form' do
        it "successfully" do
          visit 'industry_associations/mca'

          select 'foo bar', from: 'Filter by tag'

          click_button 'Filter'

          expect(page.text).to include '2 items'
          expect(page.text).to include 'link1.org'
          expect(page.text).to include  'link2.org'
          expect(page.text).not_to include  'link3.org'
        end
      end
    end
  end
end
