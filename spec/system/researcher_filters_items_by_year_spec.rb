require 'rails_helper'

RSpec.describe 'ResearcherFiltersItemsByYear', type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe 'Researcher filters items by year' do
      before do
        entity = IndustryAssociation.create!(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
        entity.items.create!(
          [
            { id: 1, document_attributes: { url: 'https://link1.org/news/1', published_at: '2018-12-21' } },
            { id: 2, document_attributes: { url: 'https://link3.org/news/2', published_at: '2019-04-01' } },
            { id: 3, document_attributes: { url: 'https://link3.org/news/3', published_at: '2016-10-15' } }
          ]
        )
      end

      it 'successfully' do
        visit 'industry_associations/mca'

        select '2018', from: 'Filter by year'

        click_button 'Filter'

        expect(page.text).to include '1 item'
        expect(page.text).to include 'link1.org'
        expect(page.text).not_to include 'link2.org'
        expect(page.text).not_to include 'link3.org'
      end
    end
  end
end
