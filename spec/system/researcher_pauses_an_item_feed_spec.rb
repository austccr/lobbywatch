require 'rails_helper'

RSpec.describe "ResearcherPausesItemFeed", type: :system do
  describe "Researcher pauses am item feed" do
    it "successfully" do
      ItemFeed.create(
        name: "AFGC News RSS (FeedBin)",
        url: "https://api.feedbin.com/v2/feeds/1602689/entries.json",
        entity: Entity.create(slug: 'afgc', name: 'Australian Food and Grocery Council'),
        item_type: "Media release",
        info_url: "https://www.afgc.org.au/category/news/"
      )

      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit 'staging/feeds'

      click_button 'Pause'

      expect(page).to have_content 'Paused'
      expect(page).to have_button 'Activate'
    end
  end
end

