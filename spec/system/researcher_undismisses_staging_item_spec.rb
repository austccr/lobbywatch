require 'rails_helper'

RSpec.describe "ResearcherUndismissesStagingItem", type: :system do
  describe "Researcher un-dismisses a staging item" do
    before do
      entity = Entity.create!(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
      StagingItem.create!(
        id: 3,
        document_attributes: { url: 'https://item.org' },
        entity: entity,
        dismissed: true
      )
    end

    it "successfully" do
      sign_in Researcher.create(email: 'freya@accr.org.au', password: 'password12345678', is_editor: true)

      visit '/staging/dismissed'

      click_button 'Return to staging'

      expect(page).to have_content 'Successfully returned item to staging.'
      expect(page).to have_content 'item.org'
    end
  end
end

