require 'rails_helper'

RSpec.describe "ResearcherViewsCompanyItems", type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher views items for a company and their members" do
      it 'successfully' do
        mca = IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca', short_name: 'MCA')
        mca.items.create!(summary: 'MCA item', id: '9')

        bhp = Company.create!(name: 'BHP', slug: 'bhp')
        bhp.items.create!(summary: 'BHP item', id: '5')

        bhp.memberships.create!(industry_association: mca)

        visit root_path
        click_link 'See all 1 company'
        click_link 'BHP'

        expect(page.text).to include '2 items'
        expect(page.text).to include 'BHP item'
        expect(page.text).to include 'MCA item'

        within '#item-5' do
          expect(page.text).to include 'BHP'
        end

        within '#item-9' do
          expect(page.text).to include 'Minerals Council of Australia'
        end

        click_link 'View items from BHP only'
        expect(page.text).to include '1 item'
        expect(page.text).to include 'BHP item'
        expect(page.text).not_to include 'MCA item'

        click_link 'Include lobbying by the industry associations BHP funds'
        expect(page.text).to include '2 items'
        expect(page.text).to include 'BHP item'
        expect(page.text).to include 'MCA item'
      end
    end
  end
end

