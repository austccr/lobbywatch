require 'rails_helper'

RSpec.describe "ResearcherViewACompanysMemberships", type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher views a company's memberships" do
      before do
        bhp = Company.create!(name: 'BHP', slug: 'bhp')

        bhp.memberships.create([
          { industry_association: IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca') },
          { industry_association: IndustryAssociation.create!(name: 'Business Council of Australia', slug: 'bca') },
          { industry_association: IndustryAssociation.create!(name: 'Queensland Resources Council', slug: 'qrc') },
          { industry_association: IndustryAssociation.create!(name: 'NSW Minerals Council', slug: 'nswca') },
          { industry_association: IndustryAssociation.create!(name: 'Resource Industry Network', slug: 'rin') }
        ])
      end

      it "successfully" do
        visit 'companies/bhp'

        click_link 'View all memberships'

        expect(page).to have_content 'Minerals Council of Australia'
        expect(page).to have_content 'Business Council of Australia'
        expect(page).to have_content 'Queensland Resources Council'
        expect(page).to have_content 'NSW Minerals Council'
        expect(page).to have_content 'Resource Industry Network'
      end
    end
  end
end
