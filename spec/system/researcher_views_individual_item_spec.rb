require 'rails_helper'

RSpec.describe "ResearcherViewsIndividualItem", type: :system do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher views an individual item" do
      it 'successfully' do
        mca = IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca', short_name: 'MCA')
        Item.create!(summary: 'This is a summary', id: '1324', tag_names: ['foo'], entity: mca)

        visit '/items/1324'

        expect(page).to have_content 'Items Item #1324'
        expect(page).to have_content 'This is a summary'
        expect(page).to have_content 'foo'
      end
    end
  end
end
