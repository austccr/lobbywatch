require 'rails_helper'

RSpec.describe "ResearcherViewsLobbygroupItems", type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher views items for a lobby group" do
      it "successfully" do
        mca = IndustryAssociation.create!(
          name: 'Minerals Council of Australia',
          short_name: 'mca',
          slug: 'mca'
        )
        mca.items.create(summary: 'MCA Item')
        Item.create(summary: 'APPEA Item')

        visit root_path
        click_link 'Minerals Council of Australia'

        expect(page.text).to include '1 item'
        expect(page).to have_content 'MCA Item'
        expect(page).not_to have_content 'APPEA Item'
      end
    end
  end
end
