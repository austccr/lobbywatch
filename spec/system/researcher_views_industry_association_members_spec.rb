require 'rails_helper'

RSpec.describe "ResearcherViewsAIndustryAssociationsMembers", type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher views an industry association's members" do
      before do
        mca = IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca')

        mca.memberships.create([
          { company: Company.create!(name: 'BHP', slug: 'bhp') }
        ])
      end

      it "successfully" do
        visit 'industry_associations/mca'

        click_link 'View all 1 member'

        expect(page).to have_content 'BHP'
      end
    end
  end
end
