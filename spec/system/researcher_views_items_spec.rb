require 'rails_helper'

RSpec.describe "ResearcherViewsItems", type: :system, sphinx: :true do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher views items" do
      it 'successfully' do
        mca = IndustryAssociation.create!(name: 'Minerals Council of Australia', slug: 'mca', short_name: 'MCA')
        mca.items.create!(summary: 'This is a summary', id: '1324')

        visit '/items'

        expect(page).to have_content 'This is a summary'
      end
    end
  end
end
