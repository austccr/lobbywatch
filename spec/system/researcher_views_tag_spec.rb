require 'rails_helper'

RSpec.describe "ResearcherViewsTag", type: :system do
  context 'in public access mode' do
    around do |example|
      ClimateControl.modify PUBLIC_ACCESS: 'true' do
        example.run
      end
    end

    describe "Researcher views tag with items" do
      it 'successfully' do
        Item.create!(
          document_attributes: { url: 'https://link1.org' },
          published_at: '2018-12-21',
          tag_names: ['foo bar', 'Energy', 'Oil & Gas']
        )
        Item.create!(
          document_attributes: { url: 'https://link2.org' },
          published_at: '2019-03-01',
          tag_names: ['foo bar', 'Energy']
        )
        Item.create!(
          document_attributes: { url: 'https://link3.org' },
          published_at: '2016-10-15',
          tag_names: ['#favs', 'Tania Constable']
        )

        visit 'tags/Tania+Constable'

        expect(page.text).to include 'Lobbying tagged Tania Constable'
        expect(page.text).not_to include 'link1.org'
        expect(page.text).not_to include 'link2.org'
        expect(page.text).to include 'link3.org'
      end
    end
  end
end
