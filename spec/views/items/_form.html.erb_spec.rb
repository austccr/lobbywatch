require 'rails_helper'

describe 'items/_form.html.erb' do
  let(:item) { Item.new }

  it 'pre-selects no lobbygroup by default' do
      Entity.create!(id: 3, name: 'Business Council of Australia', short_name: 'bca', slug: 'bca')

      render 'items/form', item: item, entity: nil, form_topic: 'foo', return_to: '/'

      expect(rendered).not_to match(/<option selected="selected"/)
  end

  context 'when there an entity is assigned' do
    before do
      Entity.create!(id: 1, name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')
      Entity.create!(id: 3, name: 'Business Council of Australia', short_name: 'bca', slug: 'bca')
    end

    it 'is pre-selected' do
      render 'items/form', item: item, entity: Entity.find(3), form_topic: 'foo', return_to: '/'

      expect(rendered).to match(/<option value="3" selected="selected">Business Council of Australia<\/option>/)
    end
  end
end
