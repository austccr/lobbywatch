require 'rails_helper'
require 'rss'

describe 'staging/index.atom.builder' do
  it 'renders staging items to atom entries' do
    entity = Entity.create!(name: 'Minerals Council of Australia', short_name: 'mca', slug: 'mca')

    assign(:staging_items, entity.staging_items.build([
      {
        document_attributes: { url: 'https://link2.org' },
        published_at: '2019-03-01',
        summary: 'This is item 2',
        created_at: DateTime.new(2019, 3, 3, 15, 2, 1).to_time
      },
      {
        document_attributes: { url: 'https://link1.org' },
        published_at: '2018-12-21',
        summary: 'This is item 1',
        created_at: DateTime.new(2018, 12, 25, 10, 6, 7).to_time
      },
      {
        document_attributes: { url: 'https://link3.org' },
        published_at: '2016-10-15',
        summary: 'This is item 3',
        created_at: DateTime.new(2018, 9, 10, 20, 9, 30).to_time
      }
    ]))

    render

    expect(RSS::Parser.parse(rendered)).to be_valid
  end
end

